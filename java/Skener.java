package kattis;

import io.Kattio;

public class Skener {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		
		int r = io.getInt();
		int c = io.getInt();
		int zr = io.getInt();
		int zc = io.getInt();
		char[][][][] art = new char[r][c][zr][zc];
		
		for(int y = 0; y < r; y++) {
			String word = io.getWord();
			for(int x = 0; x < c; x++) {
				char z = word.charAt(x);
				for(int i = 0; i < zr; i++) {
					for(int j = 0; j < zc; j++) {
						art[y][x][i][j] = z;
						
					}
				}
			}
		}
		
		
		for(int y = 0; y < r; y++) {
			for(int i = 0; i < zr; i++) {
				for(int x = 0; x < c; x++) {
					for(int j = 0; j < zc; j++) {
						io.print(art[y][x][i][j]);
					}
				}
				io.println();
			}
		}
		
		
		io.close();
	}

}

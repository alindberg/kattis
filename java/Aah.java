package kattis;

import Kattio;

public class Aah {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		
		String jm = io.getWord();
		String doctor = io.getWord();
		
		if(jm.length() < doctor.length()) {
			io.println("no");
		} else {
			io.println("go");
		}
		
		io.close();
	}

}

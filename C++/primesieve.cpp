#include <iostream>
#include <vector>

using namespace std;

//can solve faster using sqrt(n).

int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)

    int n, q;
    cin >> n >> q;

    vector<bool> isprime(n + 1, true);
    isprime[1] = 0;

    int total_primes = 0;
    for (int p = 2; p <= n; p++) {

        if (isprime[p]) {
            total_primes++;

            for (int j = 2 * p; j <= n; j += p) {
                isprime[j] = false;
            }
        }
    }

    cout << total_primes << endl;

    for (int i = 0; i < q; i++) {
        int z; 
        cin >> z;

        cout << (isprime[z] ? 1 : 0) << endl;
    }

    return 0;
}
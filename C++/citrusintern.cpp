#include <iostream>
#include "Digraph.h"

using namespace std;

const int INF = 1000001;

#define min(a,b) ((a)<(b)?(a):(b))

struct Solver {

    Digraph graph;
    int *cost;
    long *in, *out_up, *out_down;
    long ans;

    Solver(Digraph &graph, int *cost, int N, int root): graph(graph), cost(cost) {
    
        in = new long[N];
        out_up = new long[N];
        out_down = new long[N];

        solve(root);

        ans = min(in[root], out_down[root]);
    }

    void solve(int u) {
        in[u] = cost[u];
        out_up[u] = 0;
        out_down[u] = INF;
        
        if(graph.adjacent(u).size() == 0) { 
            return;
        }

        //To help find the best child to choose if you have to choose one.
        long min_diff = INF;

        for (int v : graph.adjacent(u)) {

            solve(v);

            //The tree including the root and the best subtree without any direct children.
            in[u] += out_up[v]; 

            //The tree not including the root containing either the best subtree with or without children,
            //not caring whether the root is covered.
            out_up[u] += min(in[v], out_down[v]);

            long diff = in[v] - out_down[v];

            if (diff < min_diff) {
                min_diff = diff;
            }

        }

        //The best tree rooted at u without including u, but 
        //Making sure that u is covered by at least one child.
        out_down[u] = out_up[u];
        
        if (min_diff >= 0) {
            out_down[u] += min_diff;
        }

    }
};


int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)
    
    int N;
    cin >> N;

    Digraph graph(N);
    int *cost = new int[N];
    int *indegrees = new int[N]();


    //Build the tree representing the organization structure.
    for (int i = 0; i < N; i++) {
        //Member i costs C to recruit, and has U subordinates 
        int C, U;
        cin >> C >> U;
        cost[i] = C;

        for (int j = 0; j < U; j++) {
            int u;
            cin >> u;
            graph.addEdge(i, u);
            indegrees[u]++;
        }

    }

    //Find the boss of all bosses
    int root;
    for (int i = 0; i < N; i++) {
        if(indegrees[i] == 0) {
            root = i;
            break;
        }
    }


    Solver solver(graph, cost, N, root);

    cout << solver.ans << endl;

    return 0;
}


main = do
    contents <- getContents
    let parsed = map parse . lines $ contents
    let solved = map (\(x, y) -> abs (x - y)) parsed

    mapM_ putStrLn . map show $ solved


parse :: String -> (Integer, Integer)
parse xs = (read a, read b)
    where [a, b] = words xs

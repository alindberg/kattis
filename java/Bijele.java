package kattis;

import io.Kattio;

public class Bijele {
	
	public static void main(String[] args) {
		final int[] pieces = {1, 1, 2, 2, 2, 8};
		Kattio io = new Kattio(System.in, System.out);
		for(int i = 0; i < pieces.length; i++) {
			io.print((pieces[i] - io.getInt()) + " ");
		}
		io.close();
	}

}

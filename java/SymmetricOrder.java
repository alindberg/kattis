package kattis;

import io.Kattio;

public class SymmetricOrder {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		int set = 1;
		int n = -1;
		while((n = io.getInt()) != 0) {
			int l = 0;
			int r = n - 1;
			String[] arr = new String[n];
			for(int i = 0; i < n; i++) {
				if(i % 2 == 0) {
					arr[l++] = io.getWord();
				} else {
					arr[r--] = io.getWord();
				}
			}
			
			io.println("SET " + (set++));
			for(int i = 0; i < arr.length; i++) {
				io.println(arr[i]);
			}
		}
		
		
		io.close();
	}

}

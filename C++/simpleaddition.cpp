#include <iostream>
#include <vector>
#include <string>
#include <algorithm>


using namespace std;

int ctoi(char c) {
    return c - '0';
}

int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)

    string s1, s2;

    cin >> s1 >> s2;

    if (s1.size() < s2.size()) {
        s1 = string(s2.size() - s1.size(), '0') + s1;
    } else {
        s2 = string(s1.size() - s2.size(), '0') + s2;
    }

    string result;
    int carry = 0;
    for (int i = s1.size() - 1; i >= 0; i--) {
        int a = ctoi(s1[i]);
        int b = ctoi(s2[i]);
        int c = a + b + carry;
        if (c > 9) {
            carry = 1;
            result += to_string(c - 10);
        } else {
            result += to_string(c);
            carry = 0;
        }
    }

    if (carry) {
        result.push_back('1');
    }

    reverse(result.begin(), result.end());

    cout << result << endl;

}
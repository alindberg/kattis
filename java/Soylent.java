package kattis;

import io.Kattio;

public class Soylent {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		
		int t = io.getInt();
		for(int i = 0; i < t; i++) {
			int c = io.getInt();
			io.println((int)Math.ceil(c / 400.0));
		}
		
		io.close();
	}

}

package kattis;

import java.util.Scanner;

public class SimonSays {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int n = Integer.parseInt(sc.nextLine());
		for(int i = 0; i < n; i++) {
			String line = sc.nextLine();
				
			if(line.length() >= 11 && line.substring(0, 10).equals("Simon says")) {
				System.out.println(line.substring(11, line.length()));
			}
			
		}
		
		sc.close();
		
	}

}

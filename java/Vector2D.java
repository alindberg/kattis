
public class Vector2D {
	
	private double x, y;

	public Vector2D(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public Vector2D(Vector2D a, Vector2D b) {
		this.x = b.getX() - a.getX();
		this.y = b.getY() - a.getY();
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public static Vector2D add(Vector2D u, Vector2D v) {
		return new Vector2D(u.getX() + v.getX(), u.getY() + v.getY());
	}
	
	public static double dot(Vector2D u, Vector2D v) {
		return u.getX() * v.getX() + u.getY() * v.getY();
	}

	public static double length(Vector2D v) {
		return Math.sqrt(v.getX() * v.getX() + v.getY() * v.getY());
	}
	
	public static Vector2D abs(Vector2D u, Vector2D v) {
		return new Vector2D(Math.abs(u.getX() + v.getX()), Math.abs(u.getY() + v.getY()));
	}

	public String toString() {
		return "(" + x + ", " + y + ")";
	}
}

#include <iostream>
#include <vector>
#include <string>

using namespace std;

struct Edge {
    int from, to, capacity;
    int flow = 0;

    Edge(int from, int to, int cap):
        from(from),
        to(to),
        capacity(cap)
        {}

    bool full() {
        return flow == capacity;
    }
};

typedef vector<int> vi;
typedef vector<bool> vb;
typedef vector<vector<Edge>> Graph;

//children, toys, categories.
int n, m, p, source, sink, network_size; 
bool found_sink;
Graph graph;

void explore(Edge &edge, vb &visited) {

    visited[edge.to] = true;

    if (edge.to == sink) {
        found_sink = true;
    }

    for (Edge &e : graph[edge.to]) {
        if (!visited[e.to] && !e.full() && !found_sink) {
            explore(e, visited);
        }
    }

    //reached sink, update flow and add opposite edge for certain nodes on the on the way back up. 
    if (found_sink) {
        edge.flow++;

        if (edge.from != source && edge.to != sink) {
            graph[edge.to].push_back(Edge(edge.to, edge.from, 1));
        }
    }
}

bool dfs() {

    found_sink = false;
    vb visited(network_size, false);

    for (Edge &e : graph[source]) {

        if (!e.full()) {
            explore(e, visited);
            if (found_sink) {
                break;
            }
        }
    }

    return found_sink;
}

int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)

    cin >> n >> m >> p;

    /* source:     0
       children:   1 through n
       toys:       n + 1 through n + m
       categories: n + m + 1 through n + m + p
       sink:       n + m + p + 1
    */
    network_size = n + m + p + 2;
    graph = Graph(network_size); 
    source = 0;
    sink = network_size - 1;

    //Add edge from source to all children
    for (int child = 1; child <= n; child++) {
        graph[source].push_back(Edge(source, child, 1));
    }

    //Add edge from child to toys.
    for (int child = 1; child <= n; child++) {
        int k; 
        cin >> k;

        for (int j = 0; j < k; j++) {
            int t; 
            cin >> t;

            graph[child].push_back(Edge(child, n + t, 1));
        }
    }  

    //Add edge from each toy to its category with capacity r.
    for (int i = 1; i <= p; i++) {
        int l, r; // l toys in category i with capacity r.

        cin >> l;
        vi toys(l);

        for (int j = 0; j < l; j++) {
            cin >> toys[j];
        }
        
        cin >> r;
        int cat = n + m + i;
        graph[cat].push_back(Edge(cat, sink, r));

        for (int j = 0; j < l; j++) {
            graph[n + toys[j]].push_back(Edge(n + toys[j], cat, 1));
        }
    }

    //Add edge from toy to sink for each toy belonging to no category.
    for (int toy = n + 1; toy <= n + m; toy++) {
        if (graph[toy].empty()) {
            graph[toy].push_back(Edge(toy, sink, 1));
        }
    }

    int result = 0;

    while (dfs()) {
        result++;
    }

    cout << result << endl;

    return 0;
}

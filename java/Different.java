package kattis;

import io.Kattio;

public class Different {
	
	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		
		while(io.hasMoreTokens()) {
			long a = io.getLong();
			long b = io.getLong();
			io.println(Math.abs(a - b));
		}
		
		io.close();
	}

}

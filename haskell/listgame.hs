main = do
    num <- getLine
    putStrLn $ show (f (read num :: Int))

f :: Int -> Int
f x = g x 2 0
    where 
    g :: Int -> Int -> Int -> Int
    g 1 _ c = c
    g x' d c | fromIntegral d > sqrt (fromIntegral x') = c+1
             | mod x' d == 0 = g (div x' d) d (c+1)
             | otherwise = g x' (d+1) c

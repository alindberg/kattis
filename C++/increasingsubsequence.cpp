#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include "Digraph.h"

using namespace std;

typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<bool> vb;
typedef vector<pii> vpii;


class DFS {
    Digraph graph;
    vb visited;
    int time = 0;

    public:
    vi post;

    DFS(Digraph &graph): graph(graph) {

        visited = vb(graph.vertices(), false);
        post = vi(graph.vertices());

        for (int u = 0; u < graph.vertices(); u++) {
            if (!visited[u]) {
                explore(u);
            }
        }
    }

    void explore(int u) {
        visited[u] = true;
        time++;

        for (int v : graph.adjacent(u)) {
            if (!visited[v]) {
                explore(v);
            }
        }

        post[u] = time;
        time++;
    }
};

void construct_graphs(Digraph &graph, Digraph &reversed, vi &weights) {

    for (int i = 0; i < weights.size(); i++) {
        for (int j = i + 1; j < weights.size(); j++) {
            if (weights[j] > weights[i]) {
                graph.addEdge(i, j);
                reversed.addEdge(j, i);
            }
        }
    }
}


int main() {
    ios::sync_with_stdio(false); 
    cin.tie(NULL); 

    int n;

    while (cin >> n && n) {
        
        vi weights(n);

        for (int i = 0; i < n; i++) {
            cin >> weights[i];
        }
        
        Digraph graph(n), reversed(n);
        construct_graphs(graph, reversed, weights);

        DFS dfs(graph);
        vpii pairs(n);
        
        for (int i = 0; i < n; i++) {
            pairs[i] = pii(dfs.post[i], i);
        }

        sort(pairs.rbegin(), pairs.rend());

        //dp[u]: longest increasing subseq up to vertex u.
        vi dp(n, 1);

        //prev[u]: node with longest inc subseq without u. -1 if source
        vi prev(n, -1); 
        
        //sink is the node with the longest subsequence 
        //if equal length, pick the sink with smallest weight.
        int sink;

        //length of the longest increasing subsequence.
        int maxlen = 0;

        //access nodes in order of decreasing post (start with sources)
        for (auto p : pairs) {
            int u = p.second;

            int len = 0;
            for (int v : reversed.adjacent(u)) {
                if (dp[v] > len) {
                    len = dp[v];
                    prev[u] = v;
                } else if (dp[v] == len && weights[v] < weights[prev[u]]) {
                    prev[u] = v;
                }
            }

            dp[u] += len;

            if (dp[u] > maxlen) {
                sink = u;
                maxlen = dp[u];
            } else if (dp[u] == maxlen && weights[u] < weights[sink]) {
                sink = u;
            }
        }

        //follow path from sink to source
        vi path;
        path.push_back(sink);

        while (prev[sink] != -1) {
            path.push_back(prev[sink]);
            sink = prev[sink];
        }

        cout << maxlen << " ";

        for (int i = path.size() - 1; i >= 0; i--) {
            cout << weights[path[i]] << " ";
        }
        cout << endl;
    }

    return 0;
}
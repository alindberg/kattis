main = do
    contents <- getContents
    let (a:b:c:ds) = (map read . words) contents
    putStrLn (solve a b c)
  
  
solve :: Int -> Int -> Int -> String
solve a b c | a + b == c = disp a "+" b "=" c
            | a == b + c = disp a "=" b "+" c
            | a - b == c = disp a "-" b "=" c
            | a == b - c = disp a "=" b "-" c
            | a * b == c = disp a "*" b "=" c
            | a == b * c = disp a "=" b "*" c
            | div a b == c = disp a "/" b "=" c
            | a == div b c = disp a "=" b "/" c
            | otherwise = "wtf"
            
    where disp x op1 y op2 z = show x ++ op1 ++ show y ++ op2 ++ show z
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int minweight(int weights[], int n) {
    
    vector<int> sortedweights;
    sortedweights.assign(weights, weights + 2 * n);

    sort(sortedweights.begin(), sortedweights.end());
    sortedweights.erase(std::unique(sortedweights.begin(), sortedweights.end()), sortedweights.end());

    int lo = 0;
    int hi = n;

    int minweight = sortedweights[hi - 1];

    while (hi > lo) {

        int mid = (lo + hi) / 2;
        bool works = true;

        //remember on which row you picked up a weight
        bool row1 = true;

        //the weight to roll
        int cur_weight = 0;

        for (int i = 0; i < n * 2 ; i++) {
            if (weights[i] > sortedweights[mid]) {

                //Not currently holding a weight
                if (cur_weight == 0) { 

                    //try to roll this
                    cur_weight = weights[i];

                    //cout << "picked up " << cur_weight << " on row ";

                    //this weight was picked up at row 2.
                    if (i >= n) {
                        row1 = false;
                        //cout << "2" << endl;
                    } //else row 1

                } else {
                    //cout << "trying to put down weight " << weights[i] << " and i = " << i << " and n = " << n;
                    //the weight is picked up and put down on the same row. put it down
                    if (cur_weight == weights[i] && (row1 && i < n || !row1 && i >= n)) {
                        cur_weight = 0;
                        //cout << "it worked" << endl;
                    } else {
                        works = false;
                        //cout << "it didnt work" << endl;
                        break;
                    }
                }
            }
        }

        //Try even smaller
        if (works) {
            minweight = sortedweights[mid];
            //cout << "updated minweight to " << minweight << endl;
            hi = mid; 

            //cout << " mid = " << mid << ", lo = " << lo << ", hi = " << hi << endl;

            if (mid == 0) minweight = 0;

        //try a larger
        } else { 
            //cout << sortedweights[mid] << " did not work, higher" << endl;
            lo = mid + 1;
        }
    }

    return minweight;
}

int main() {

    int n;
    cin >> n;

    int weights[2*n];

    for(int x = 0; x < 2*n; x++) {
        cin >> weights[x];
    }

    cout << minweight(weights, n) << endl;

    return 0;
}
package kattis;

import io.Kattio;

public class GrassSeed {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		
		double c = io.getDouble(); //cost of seed for one m^2 of lawns
		int l = io.getInt(); //number of lawns
		double sum = 0;
		for(int i = 0; i < l; i++) {
			double w = io.getDouble();
			double h = io.getDouble();
			
			sum += w * h * c;

		}
		io.println(sum);
		io.close();
	}

}

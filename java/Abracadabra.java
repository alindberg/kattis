package kattis;

import Kattio;

public class Abracadabra {
	
	public static void main(String[] args) {
		Kattio io = new Kattio(System.in,System.out);
		int n = io.getInt();
		
		for(int i = 0; i < n; i++) {
			io.println((i + 1) + " Abracadabra");
		}
		io.close();
	}

}

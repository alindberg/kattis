package kattis;

import io.Kattio;

public class Modolo {

	
	public static void main(String[] args) {
		int[] cs = new int[42];
		
		Kattio io = new Kattio(System.in, System.out);
		for(int i = 0; i < 10; i++) {
			int k = Math.floorMod(io.getInt(), 42);
			if(cs[k] == 0) {
				cs[k]++;
			}
		}
		int dist = 0;
		for(int l : cs) {
			if(l == 1) {
				dist++;
			}
		}
		
		System.out.println(dist);
		io.close();
	}
}

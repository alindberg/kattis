import sys

def knap(weights):
    W = 1000
    cap = 2001
    n = len(weights)

    K = [[0] * cap for r in range(n + 1)]

    for j in range(1, n + 1):
        wj = weights[j-1]

        for w in range(1, cap):
            if wj > w:
                K[j][w] = K[j-1][w]
            else:
                K[j][w] = max(K[j-1][w], K[j-1][w - wj] + wj)

    bestsum = 0
    bestdist = W + 1

    for i in range(cap):

        d = abs(K[n][i] - W)
        if d < bestdist:
            bestdist = d
            bestsum = K[n][i]
        elif d == bestdist:
            bestsum = max(bestsum, K[n][i])

            
    return bestsum


n = int(sys.stdin.readline())
weights = []

for i in range(n):
    w = int(sys.stdin.readline())
    weights.append(w)

print(int(knap(weights)))

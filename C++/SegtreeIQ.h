#ifndef SegtreeIQ_H
#define SegtreeIQ_H

#include <vector>
#include <string>

using namespace std;

class SegtreeIQ {
    int N, offs;
    long *A;

public:
    SegtreeIQ(int n);
    long getValue(int i);
    void update(int i, int j, int diff);
};

#endif
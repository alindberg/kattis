--import qualified Data.ByteString as B
--import qualified Data.ByteString.Char8 as BC

main = solve

solve :: IO ()
solve = do
    line <- getLine
    case words line of 
        ["0"] -> return ()
        (s:l:_) -> do 
            --putStrLn (show (count s l)) 
            
            {- println a b c, where
                a = count for S without any changes

                b = all unique strings made by deleting 1 char from S
                    e.g ACG => AG, AC or GC
                    
                c = all unique strings made by inserting 1 char in S
                    e.g. AG => AAG, GAG, CAG, TAG, AGG, ACG, ATG, AGA, AGC, AGT
                
                if 2 or more different modifications of S result in the same string,
                    count that occurrence only once..
            -}

            solve

count :: String -> String -> Int
count x y = 0
package kattis;

import io.Kattio;

public class Filip {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		
		String A = new StringBuilder(io.getWord()).reverse().toString();
		String B = new StringBuilder(io.getWord()).reverse().toString();
		
		System.out.println(Math.max(Integer.parseInt(A), Integer.parseInt(B)));
		
		
		
		
		io.close();
	}

}

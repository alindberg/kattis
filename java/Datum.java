package kattis;

import java.time.LocalDate;

import io.Kattio;

public class Datum {

	public static void main(String[] args) {
		
		Kattio io = new Kattio(System.in, System.out);
		int date = io.getInt();
		int month = io.getInt();
		int year = 2009;
		//LocalDateTime timePoint = LocalDateTime.now();
		String k = LocalDate.of(year, month, date).getDayOfWeek().toString();
		
		io.print(k.charAt(0));
		for(int i = 1; i < k.length(); i++) {
			io.print(Character.toLowerCase(k.charAt(i)));
		}
		io.println();
		

		io.close();
	}

}

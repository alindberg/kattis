package kattis;

import io.Kattio;

public class Faktor {
	
	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		
		int A = io.getInt(); //articles to publicsh
		
		int B = io.getInt(); //Impact required
		
		System.out.println(A * B - A + 1);
		
		io.close();
	}

}

#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

/**
long bf(vector<int> &fruits) {

    int N = fruits.size();

    long total_weight = 0;

    for (long S = 0; S < pow(2, N); S++) {
        long basket_weight = 0;
        for (int j = 0; j < N; j++) {
            if (S & (1 << j)) {
                basket_weight += fruits[j];
                //j is in the set.
            }
        }
        
        if (basket_weight < 200) {
            total_weight += basket_weight;
        }
    }

    return total_weight;
}
*/

//Any subset of size 4 must be >= 200.
void remove_small(vector<int> &fruits, long long &total_sum) {
    
    for (int i = 0; i < fruits.size(); i++) {

        //Know for certain each fruit is less than 200.
        total_sum -= fruits[i];

        for (int j = i + 1; j < fruits.size(); j++) {
            
            if (fruits[i] + fruits[j] < 200) {
                total_sum -= fruits[i] + fruits[j];
            }

            for (int k = j + 1; k < fruits.size(); k++) {
            
                if (fruits[i] + fruits[j] + fruits[k] < 200) {
                    total_sum -= fruits[i] + fruits[j] + fruits[k];
                }
            }
        }
    }

}


int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)
    
    int N; //number of fruits that the company sells.
    cin >> N;
    vector<int> fruits;

    int fruitsum = 0;

    for (int i = 0; i < N; i++) {

        int fruit;
        cin >> fruit;

        fruitsum += fruit;

        if (fruit < 200) {
            fruits.push_back(fruit);
        }
    }
    
    //considering all possible subsets: each fruit occur 2^(n-1) times.
    //factor out and get this total_sum:
    long long total_sum = fruitsum * pow(2, N - 1);

    //remove the baskets too small by brute force. 
    remove_small(fruits, total_sum);

    cout << total_sum << endl;

    return 0;
}

#include <iostream>
#include <vector>
#include <climits>
#include <queue>

using namespace std;

typedef long long ll;
typedef pair<ll, int> pii;
typedef vector<int> vi;
typedef vector<ll> vl;
typedef vector<bool> vb;
typedef vector<pii> vpii;
typedef vector<vpii> vvpii;

#define weight first
#define vertex second
const ll inf = LONG_LONG_MAX / 2;

void solve(vvpii &adj, int n, int m, int q, int s) {

    vl dist(n, inf);
    dist[s] = 0;

    for (int i = 0; i < n - 1; i++) {
        for (int u = 0; u < n; u++) {
            if (dist[u] < inf) {
                for (auto &v : adj[u]) {
                    dist[v.vertex] = min(dist[v.vertex], dist[u] + v.weight);
                }
            }
        }
    }

    while (true) {
        int updated = 0;
        for (int u = 0; u < n; u++) {

            if (dist[u] == inf) continue;

            for (auto &v : adj[u]) {

                if (dist[v.vertex] != -inf && dist[u] + v.weight < dist[v.vertex]) {
                    dist[v.vertex] = -inf;
                    updated++;
                }
            }
        }
        if (updated == 0) break;
    }
    
    for (int i = 0; i < q; i++) {
        int t;
        cin >> t;
        if (dist[t] == inf) {
            cout << "Impossible\n";
        } else if (dist[t] == -inf) {
            cout << "-Infinity\n";
        } else {
            cout << dist[t] << "\n";
        }
    }

    cout << endl;
}


int main() {
    ios::sync_with_stdio(false); 
    cin.tie(NULL); 
    cout.tie(NULL);

    int n, m, q, s;
    cin >> n >> m >> q >> s;

    while (n) {
        vvpii adj(n);

        for (int i = 0; i < m; i++) {
            int u, v, w;
            cin >> u >> v >> w;
            adj[u].push_back(pii(w, v));
        }
        
        solve(adj, n, m, q, s);

        cin >> n >> m >> q >> s;
    }

    
    return 0;
}
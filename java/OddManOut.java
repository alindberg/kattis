package kattis;

import java.util.Arrays;

import io.Kattio;

public class OddManOut {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);

		int n = io.getInt();
		for(int i = 0; i < n; i++) {
			int g = io.getInt(); //guests..
			int[] nums = new int[g];
			for(int j = 0; j < g; j++) {
				nums[j] = io.getInt();
			}
			Arrays.sort(nums);
			int num = -1;
			for(int k = 0; k < g - 1; k+=2) {
				if(nums[k] != nums[k + 1]) {
					num = nums[k];
					break;
				}
			}
			if(num == -1) 
				num = nums[nums.length - 1];
			
			io.println("Case #" + (i + 1) + ": " + num);

		}
		
		io.close();
	}

}

#ifndef Digraph_H
#define Digraph_H

#include <vector>
#include <string>

using std::vector;
using std::string;

class Digraph {

    const int v;
    int e;
    vector<vector<int>> adj;

public:
    Digraph(int v);
    void addEdge(int u, int v);
    vector<int> adjacent(int v);
    int vertices();
    int edges();
    string show();
};


#endif
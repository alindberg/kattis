import sys

'''
mem = {0 : 0, 1 : 2}

for k in range(2, 100):
    r = mem[k - 1] + 2**(k - 2) + (2**(k - 2) - mem[k - 2])
    mem[k] = r
    print(mem[k])
'''


def fibb(n): #generate all fibbonacci up to n
    fibbs = [1, 1]
    
    for j in range(2, n):
        f = fibbs[j - 2] + fibbs[j - 1]
        fibbs.append(f)

    return fibbs

fb = fibb(10003)
n = int(sys.stdin.readline())

for i in range(n):
    j = int(sys.stdin.readline())
    print(fb[j + 1] % (10**9 + 7))

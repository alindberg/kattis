package kattis;

import Kattio;

public class DiceCup {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		
		int n = io.getInt();
		int m = io.getInt();

		int[] count = new int[m + n + 1];
		
		for(int i = 0; i < n; i++) {
			for(int j = 0; j < m; j++) {
				count[(i + 1) + (j + 1)]++;
			}
		}
		
		int iom = 0;
		for(int i = 0; i < count.length; i++) {
			if(count[i] > count[iom]) iom = i;
		}
		
		for(int i = 0; i < count.length; i++) {
			if(count[i] == count[iom]) System.out.println(i);
		}
		io.close();
	}
	
	static void printarr(int[] arr) {
		for(int y = 0; y < arr.length; y++) {
			System.out.print(arr[y] + " ");
		}
	}
	
	static void print2d(int[][] arr) {
		for(int y = 0; y < arr.length; y++) {
			for(int x = 0; x < arr[0].length; x++) {
				System.out.print(arr[y][x] + "  ");
			}
			System.out.println();
		}
	}

}

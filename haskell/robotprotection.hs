import Numeric (showFFloat)
import Data.Maybe
import Control.Monad (replicateM, when)
import Data.List (sort)
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as BC

type Point = (Int, Int)
type Polygon = [Point]

main = readbeacons

readbeacons = do
    beacons <- B.getLine
    run (parseInt beacons)

run :: Int -> IO ()
run 0 = return ()
run b = do
    --read the coordinates of all beacons
    ps <- getLines b

    let sorted = sort (map readPoint ps)

    if b < 3 then putStrLn "0.0" >> readbeacons
    else do
        let lower = init $ hull sorted
        let upper = init $ hull (reverse sorted)

        let area = calcarea $ reverse (lower ++ upper)
        
        putStrLn $ format area 1
        readbeacons

hull :: Polygon -> Polygon
hull (p0:p1:ps) = go [p1, p0] ps
    where go poly [] = poly
          go [p] (x:xs) = go [x, p] xs
          go (p1:p0:ps) (x:xs) | clockwise p0 p1 x = go (p0:ps) (x:xs)
          go (p1:p0:ps) (x:xs) = go (x:p1:p0:ps) xs

calcarea :: Polygon -> Double
calcarea vecs = 0.5  * (fromIntegral $ go vecs 0)
    where go [] s = s
          go [v] s = s + det v (head vecs)
          go (v1:v2:vs) s = go (v2:vs) (s + det v1 v2)

clockwise :: Point -> Point -> Point -> Bool
clockwise (x0, y0) (x1, y1) (x2, y2) = det p1 p2 <= 0
    where p1 = (x1 - x0, y1 - y0)
          p2 = (x2 - x0, y2 - y0)

det :: Point -> Point -> Int
det (x1, y1) (x2, y2) = x1 * y2 - x2 * y1

parseInt :: B.ByteString -> Int
parseInt = fst . fromJust . BC.readInt

readPoint :: B.ByteString -> Point
readPoint bs = (parseInt x, parseInt y)
    where [x, y] = BC.split ' ' bs
    
getLines :: Int -> IO [B.ByteString]
getLines n = replicateM n B.getLine

format :: RealFloat a => a -> Int -> String
format x decimals = showFFloat (Just decimals) x ""
#include <iostream>
#include <vector>
#include <climits>
#include <algorithm>
#include <queue>

using namespace std;

#define inf INT_MAX
#define weight first
#define vertex second

typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;

typedef pair<int, int> pii;
typedef vector<pii> vpii;
typedef vector<vpii> vvpii;

//Prim's algorithm
int mst(vvpii &adj) {

    int n = adj.size();
    vi prev(n, inf);
    vi cost(n, inf);
    vb visited(n, false);

    //Pick any initial node.
    cost[0] = 0;

    priority_queue<pii, vector<pii>, greater<pii>> pq;
    pq.push(pii(0, 0));

    while (!pq.empty()) {
        int u = pq.top().vertex;
        pq.pop();
        visited[u] = true;
        
        for (auto &p : adj[u]) {
            int w = p.weight;
            int v = p.vertex;

            if (w < inf && !visited[v] && cost[v] > w) {
                cost[v] = w;
                prev[v] = u;
                pq.push(pii(w, v));
            }
        }
    }

    return *max_element(cost.begin(), cost.end());
}


int main() {
    ios::sync_with_stdio(false); 
    cin.tie(NULL); 

    int n, m;
    cin >> n >> m;

    vvpii adj(n);

    for (int i = 0; i < m; i++) {
        int u, v, w;
        cin >> u >> v >> w;

        adj[u].push_back(pii(w, v));
        adj[v].push_back(pii(w, u));
    }
    
    int max_edge = mst(adj);

    if (max_edge == inf) {
        cout << "IMPOSSIBLE\n";
    } else {
        cout << max_edge << endl;
    }

    return 0;
}

import Data.Int

main = do
    contents <- getContents
    let (t:ds) = (map read . words) contents
    let ans = map solve ds
    sequence_ (map print ans)
  
solve :: Int -> Int64
--solve ds = mod (8 * 9^(ds - 1)) (10^9 + 7)
solve d = mod (8 * powmod m 9 (d - 1)) m
    where m = (10^9 + 7)

powmod :: Int64 -> Int64 -> Int -> Int64
powmod m = powmod'
    where powmod' _ 0 = 1
          powmod' a i | even i = rec
                      | otherwise = mod (a*rec) m
              where rec = powmod' (mod (a*a) m) (div i 2)

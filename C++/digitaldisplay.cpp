#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include <algorithm>

using namespace std;

typedef vector<int> vi;
typedef vector<bool> vb;
typedef vector<vb> vvb;

typedef vector<string> vs;

typedef pair<int, int> pii;
typedef vector<pii> vii;

typedef bool (*boolfunc) (int x, int y);

#define x first
#define y second

#define num_nums 10
#define rows 7
#define digit_cols 5
#define output_cols 29

bool zero (int x, int y) {return x % 4 == 0 || y % 6 == 0;}
bool one  (int x, int y) {return x == 4;}
bool two  (int x, int y) {return x == 4 && y < 3 || y % 3 == 0 || x == 0 && y > 3;}
bool three(int x, int y) {return x == 4 || y % 3 == 0;}
bool four (int x, int y) {return x % 4 == 0 && y < 3 || y == 3 || x == 4 && y > 3;}
bool five (int x, int y) {return x == 0 && y < 3 || y % 3 == 0 || x == 4 && y > 3;}
bool six  (int x, int y) {return x == 0 && y < 3 || y % 3 == 0 || x % 4 == 0 && y > 3;}
bool seven(int x, int y) {return x == 4 || y == 0;}
bool eight(int x, int y) {return x % 4 == 0 || y % 3 == 0;}
bool nine (int x, int y) {return x % 4 == 0 && y < 3 || y % 3 == 0 || x == 4 && y > 3;}

bool corner    (int x, int y) {return x % 4 == 0 && y % 3 == 0;}
bool vertical  (int x, int y) {return x % 4 == 0 && y % 3 != 0;}
bool horizontal(int x, int y) {return x % 4 != 0 && y % 3 == 0;}

vvb get_nums() {

    boolfunc funcs[num_nums] = {zero, one, two, three, four, five, six, seven, eight, nine};

    vvb nums(num_nums, vb(rows * digit_cols, false));

    for (int i = 0; i < num_nums; i++) {
        for (int y = 0; y < rows; y++) {
            for (int x = 0; x < digit_cols; x++) {
                nums[i][x + y * rows] = funcs[i](x, y);
            }
        }
    }
    return nums;
}


int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)
    cout.tie(NULL);

    vvb funcs = get_nums(); 
    
    int indices[4] = {0, 1, 3, 4};
    int offset[4] = {0, 7, 17, 24};
    
    string input;
    cin >> input;
    while (input.compare("end") != 0) {

        vs output(rows, string(output_cols, ' '));
        output[2][14] = 'o';
        output[4][14] = 'o';

        for (int i = 0; i < 4; i++) {

            int digit = (int)input[indices[i]] - '0';

            for (int y = 0; y < rows; y++) {
                for (int x = 0; x < digit_cols; x++) {

                    if (funcs[digit][x + y * rows]) {

                        int dx = x + offset[i];

                        if (corner(x, y)) {
                            output[y][dx] = '+';
                        } else if (vertical(x, y)) {
                            output[y][dx] = '|';
                        } else if (horizontal(x, y)) {
                            output[y][dx] = '-';
                        } 
                    }
                }
            }
        }

        for (int y = 0; y < rows; y++) {
            cout << output[y] << endl;
        }
        cout << "\n\n";
        
        cin >> input;
    }

    cout << "end" << endl;
    return 0;
}
package kattis;

import io.Kattio;

public class Planina {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
			
//		int n = io.getInt();
		io.println(points(io.getInt()));
		
		
		io.close();
	}

	
	static int points(int n) {
		return (int) Math.pow(Math.pow(2, n) + 1, 2);

	}
}

#include <iostream>
#include <vector>
#include <string>
#include "Digraph.h"

using namespace std;

typedef vector<int> vi;
typedef vector<bool> vb;

bool foundbook(int &i, Digraph &graph, vi &usedby, vb &visited) {

    for (int j : graph.adjacent(i)) {
        //person i likes book j
        if (!visited[j]) {

            visited[j] = true;

            if (usedby[j] == -1 || foundbook(usedby[j], graph, usedby, visited)) {
                usedby[j] = i;
                return true;
            }
        }
    }
    return false;
}

int count_matches(Digraph &graph) {

    int people = graph.vertices();

    //Which person currently has this book, -1 if none.
    vi usedby(people, -1);

    int matches = 0;
    for (int i = 0; i < people; i++) {
        vb visited = vb(graph.vertices(), false);
        if (foundbook(i, graph, usedby, visited)) {
            matches++;
        }
    }

    return matches;
}

int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)

    //people, decl of interests
    int n, m;
    cin >> n >> m;

    Digraph graph(n);

    for (int i = 0; i < m; i++) {
        int u, v;
        cin >> u >> v;
        graph.addEdge(u, v);
    }

    int count = count_matches(graph);

    cout << (count == n ? "YES" : "NO") << endl;

    return 0;
}

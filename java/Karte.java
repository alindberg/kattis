package kattis;

import io.Kattio;

public class Karte {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		
		String labels = io.getWord();
		boolean[] deck = new boolean[53];
		
		for(int i = 0; i < labels.length() - 2; i += 3) {
			int m = 13 * mult(labels.charAt(i));

			int num = Integer.parseInt(labels.substring(i + 1, i + 3));
			if(deck[m + num]) {
				deck[0] = true;
				break;
			} else {
				deck[m + num] = true;
			}	
		}
		
		if(deck[0]) {
			io.println("GRESKA");
		} else {			
			for(int i = 0; i < 4; i++) {
				int missing = 0;
				for(int j = 1; j <= 13; j++) {
					if(!deck[i * 13 + j]) missing++;
				}
				io.print(missing + " ");
				
			}
		}
		
		// i = 0, j = 1 => deck[1 * (0 + 1)] = deck[1]
		// i = 0, j = 2 => deck[2 * (0 + 1)] = deck[2]
		//..
		//i = 1, j = 1 => deck[1 * (1 + 1)] = deck[
		
		io.close();
	}
	
	static char mult(char x) {
		if (x == 'P') {
			return 0;
		} else if(x == 'K') {
			return 1;
		} else if(x == 'H'){
			return 2;
		} else if(x == 'T') {
			return 3;
		} else {
			throw new IllegalArgumentException("wtf");
		}
	}

}

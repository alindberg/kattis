#include <iostream>
#include <vector>
#include <queue>
#include <functional>

using namespace std;

typedef pair<int, int> pii;

int INT_MAXX = 1000001;

struct Cell {
    int x, y, depth;
    Cell(int x, int y, int depth): x(x), y(y), depth(depth) {}

    bool operator>(const Cell &rhs) const {
        return depth > rhs.depth;
    }
};

vector<pii> neighborsOf(int x, int y, int rows, int cols) {
    pair<int, int> candidates[] = {
        make_pair(x, y - 1),
        make_pair(x - 1, y),
        make_pair(x + 1, y),
        make_pair(x, y + 1)};

    vector<pii> neighbors;
    for (pii p : candidates) {
        int dx = p.first;
        int dy = p.second;

        if (dy >= 0 && dy < rows && dx >= 0 && dx < cols) {
            neighbors.push_back(p);
        }
    }

    return neighbors;
}

int min_mud(int **grid, int **depth, bool **visited, int rows, int cols) {

    priority_queue<Cell, vector<Cell>, greater<Cell>> queue;

    for (int y = 0; y < rows; y++) {
        queue.push(Cell(0, y, grid[y][0]));
        depth[y][0] = grid[y][0];
    }

    while (!queue.empty()) {
        Cell cell = queue.top();
        queue.pop();
 
        if (visited[cell.y][cell.x]) continue;
       
        visited[cell.y][cell.x] = true;

        for (pii neighbor : neighborsOf(cell.x, cell.y, rows, cols)) {

            int nx = neighbor.first;
            int ny = neighbor.second;
            
            if(visited[ny][nx]) continue;

            int mud = max(depth[cell.y][cell.x], grid[ny][nx]);

            //found a better route
            if (depth[ny][nx] > mud) {
                depth[ny][nx] = mud;            
                queue.push(Cell(nx, ny, mud)); 
            }
        }
    }

    int minmud = INT_MAXX;
    for (int y = 0; y < rows; y++) {
        if (depth[y][cols-1] < minmud) minmud = depth[y][cols-1];
    }

    return minmud;
}



int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)


    int rows, cols;
    cin >> rows >> cols;

    int **grid = new int*[rows];
    int **depth = new int*[rows];
    bool **visited = new bool*[rows];

    for (int y = 0; y < rows; y++) {

        grid[y] = new int[cols];
        depth[y] = new int[cols];
        visited[y] = new bool[cols];

        for (int x = 0; x < cols; x++) {

            cin >> grid[y][x];

            visited[y][x] = false;
            depth[y][x] = INT_MAXX;
        }
    }

    cout << min_mud(grid, depth, visited, rows, cols) << endl;
    
    return 0;
}

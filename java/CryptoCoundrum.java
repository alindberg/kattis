package kattis;

import io.Kattio;

public class CryptoCoundrum {

	public static void main(String[] args) {

		Kattio io = new Kattio(System.in, System.out);
		
		
		String w = io.getWord();
		
		int days = 0;
		int k = 1;
		for(int i = 0; i < w.length(); i++) {
			if(k == 1 && w.charAt(i) != 'P') days++;
			if(k == 2 && w.charAt(i) != 'E') days++;
			if(k == 3 && w.charAt(i) != 'R') days++;
			
			if(k == 3) k = 1;
			else k++;
		}
		
		io.println(days);
		
		io.close();
	}

}

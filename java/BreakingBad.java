
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

public class BreakingBad {

    public static void main(String[] args) {
        
        Kattio io = new Kattio(System.in, System.out);
        BreakingBad bad = new BreakingBad();
        Map<String, Integer> map = new HashMap();

        int n = io.getInt();
        String[] items = new String[n];
        for(int i = 0; i < n; i++) {
            items[i] = io.getWord();
            map.put(items[i], i);
        }

        
        Graph g = new Graph(n);
        
        
        int p = io.getInt();

        for(int i = 0; i < p; i++) {
   
            int u = map.get(io.getWord());
            int v = map.get(io.getWord());
            
            g.addEdge(u, v);
        }

        
        DFS dfs = new DFS(g);

        List<String> walter = new ArrayList<>();
        List<String> jesse = new ArrayList<>();
       
        if(!dfs.isBipartite()) {
            io.println("impossible");
        } else {
            for (String s : items) {
                int v = map.get(s);
                if(dfs.color(v)) {
                    walter.add(s);
                } else {
                    jesse.add(s);
                }
            }
        }

        bad.show(walter);
        bad.show(jesse);

        io.close();
    }

    public void show(List<String> list) {
        for (int i = 0; i < list.size(); i++) {

            if (i == list.size() - 1) {
                io.println(list.get(i));
            } else {
                io.print(list.get(i) + " ");
            }
        }
    }

}

class DFS {

    private boolean[] visited, color;
    private boolean bipartite = true;

    public DFS(Graph g) {
        this.visited = new boolean[g.V()];
        this.color = new boolean[g.V()];
        
        for(int v = 0; v < g.V(); v++) {
            if(!visited[v]) {
                explore(g, v);
            }
        }
    }

    private void explore(Graph g, int v) {
        visited[v] = true;

        for (int u : g.adj(v)) {

            if(!visited[u]) {
                color[u] = !color[v];
                explore(g, u);
            } else if (color[v] == color[u]) {
                this.bipartite = false; 
            }

        }
    }

    public boolean color(int v) {
        return color[v];
    }

    public boolean isBipartite() {
        return bipartite;
    }
}
#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

long bfw(vector<int> &fruits) {

    int N = fruits.size();

    long total_weight = 0;

    for (long S = 0; S < pow(2, N); S++) {
        long basket_weight = 0;
        for (int j = 0; j < N; j++) {
            if (S & (1 << j)) {
                basket_weight += fruits[j];
                //j is in the set.
            }
        }
        
        if (basket_weight < 200) {
            total_weight += basket_weight;
        }
    }

    return total_weight;
}


int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)
    
    int N; //number of fruits that the company sells.
    cin >> N;
    vector<int> fruits;

    int fruitsum = 0;

    for (int i = 0; i < N; i++) {

        int fruit;
        cin >> fruit;

        fruitsum += fruit;

        if (fruit < 200) {
            fruits.push_back(fruit);
        }
    }
    
    int a = fruitsum * pow(2, N - 1);
    int k = bfw(fruits);

    cout << (a - k) << endl;

    return 0;
}

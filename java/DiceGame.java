package kattis;

import io.Kattio;

public class DiceGame {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);

		int a1 = io.getInt();
		int b1 = io.getInt();
		
		int a2 = io.getInt();
		int b2 = io.getInt();

		double gunnar = (a1 + b1 + a2 + b2) / 2.0;
		
		int x1 = io.getInt();
		int y1 = io.getInt();
		int x2 = io.getInt();
		int y2 = io.getInt();
		
		double emma = (x1 + y1 + x2 + y2) / 2.0;
		
		if(gunnar > emma) {
			io.println("Gunnar");
		} else if(gunnar == emma) {
			io.println("Tie");
		} else {
			io.println("Emma");
		}
		
		io.close();
	}


}

package kattis;

import io.Kattio;

public class Autori {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		String[] names = io.getWord().split("-");
		StringBuilder sb = new StringBuilder();
		for(String s : names) {
			sb.append(s.charAt(0));
		}
		
		io.println(sb.toString());
		io.close();
	}

}

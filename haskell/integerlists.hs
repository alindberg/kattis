
main = do
    contents <- getContents
    let (t:instr) = words contents
    let z = map solve (spl instr)
    mapM_ putStrLn z
    
    
solve :: (String, String) -> String
solve ([], ls) = ls
solve ('R':xs, '[':ls) = let ']':ls' = reverse ls in solve (xs, '[':ls' ++ "]")
solve ('D':_, []) = "error"
solve ('D':xs, '[':l:',':ls) = solve (xs, '[':ls)
    

    
spl :: [String] -> [(String, String)]
spl (x:_:z:[]) = (x, z):[]
spl (x:_:z:xs) = (x, z):spl xs 
    
    

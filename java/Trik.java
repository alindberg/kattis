package kattis;

import io.Kattio;

public class Trik {

	public static void main(String[] args) {
		
		Kattio io = new Kattio(System.in, System.out);
		
		
		int[] v = {1, 0, 0};
		String s = io.getWord();
		for(char c : s.toCharArray()) {
			if(c == 'A') {
				int temp = v[0];
				v[0] = v[1];
				v[1] = temp;
			} else if(c == 'B') {
				int temp = v[1];
				v[1] = v[2];
				v[2] = temp;
			} else {
				int temp = v[2];
				v[2] = v[0];
				v[0] = temp;
			}
				
		}
		
		for(int i = 0; i < v.length; i++) {
			if(v[i] == 1) {
				System.out.println(i + 1);
			}
		}
		
		io.close();
	}

}

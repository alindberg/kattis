package kattis;

import io.Kattio;

public class Zanzibar {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in,System.out);
		
		int t = io.getInt();
		for(int i = 0; i < t; i++) {
			
			int lowerB = 0;
			int first = io.getInt();
			int second;
			while((second = io.getInt()) != 0) {
				int diff = second - 2 * first;
				if(diff > 0) {//case where new population is more than twice as much as last year.
					lowerB += diff;
				}
				first = second;
				
			}
			io.println(lowerB);
		}
		
		io.close();
	}

}

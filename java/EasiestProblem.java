package kattis;

import io.Kattio;

public class EasiestProblem {

	public static void main(String[] args) {
	
		Kattio io = new Kattio(System.in, System.out);
		int N = io.getInt();
		while(N != 0) {
			int sumN = sumOfDigits(N);
			
			int p = 11;
			while(sumOfDigits(N * p) != sumN) {
				p++;			
			}
			io.println(p);
			N = io.getInt();
		}
		io.close();
	}
	
	
	static int sumOfDigits(int k) {
		int sum = 0;
		for(char z : String.valueOf(k).toCharArray()) {
			sum += Character.getNumericValue(z);
		}
		return sum;
	}

}

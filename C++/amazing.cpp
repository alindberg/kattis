#include <iostream>
#include <string>
#include <vector>
#include <deque>

using namespace std;


struct Pos {
    int x, y;

    Pos(): x(0), y(0) {}

    Pos(int x, int y): x(x), y(y) {}

    bool operator == (const Pos& rhs) const {
        return this->x == rhs.x && this->y == rhs.y;
    }
};

struct Door {
    string dir;
    Pos pos;

    Door() {}
    Door(string dir, Pos pos): dir(dir), pos(pos) {}
};

struct Room {
    Pos pos;
    deque<Door> doors;

    Room() {}
    Room(Pos pos): pos(pos) {}
};

class Maze {
    int width, height;
    vector<vector<Room>> rooms;

    public:
    Maze(int width, int height): 
        width(width),
        height(height), 
        rooms(vector<vector<Room>>(height, vector<Room>(width))) {

            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {

                    Pos pos(x, y);
                    Room room(pos);

                    room.doors.push_back(Door("right", Pos(x + 1, y)));
                    room.doors.push_back(Door("up", Pos(x, y + 1)));
                    room.doors.push_back(Door("down", Pos(x, y - 1)));
                    room.doors.push_back(Door("left", Pos(x - 1, y)));

                    rooms[y][x] = room;
                }
            }
        }

    Room& get(Pos p) {
        return rooms[p.y][p.x];
    }
};

void explore(Maze& maze, Pos current, Pos prev) {

    Room& room = maze.get(current);

    string reply;

    if (room.doors.empty()) {
        cout << "no way out" << endl;
        getline(cin, reply);
        return;
    }

    if (!(current == prev)) {
        for (int i = 0; i < room.doors.size() - 1; i++) {
            if (room.doors[i].pos == prev) {

                room.doors.push_back(room.doors[i]);
                room.doors.erase(room.doors.begin() + i);
                break;
            }
        }
    }

    Door& door = room.doors.front();
    room.doors.pop_front();
    
    Pos next = current;

    //cout << room.show() << endl;
    //cout << "Door: " << door.show() << endl;
    //cout << "Next: " << next.show() << endl;

    //avoid getting stuck
    if(maze.get(door.pos).doors.empty()) {

        if(room.doors.empty()) {
            cout << door.dir << endl;
            getline(cin, reply);

            if (reply == "solved" || reply == "wrong") return; 

            if(reply == "ok") {
                next = door.pos;
            } else if (reply == "wall") {

            }

        } else {
            
            //save this door for last
            //this'll create an infinite loop for sure
            room.doors.push_back(door);

            //cout << "this thing thats not dealt with happened, what to do with: " << door.pos.show() << endl;
            //If the room you are currently standing in is not fully visited, you might have to go here at some point
        }

    } else {
        
        cout << door.dir << endl;
        getline(cin, reply);
        
        if (reply == "ok") {  

            //walk in that direction.
            next = door.pos;

            //cout << "Received ok, setting next to: " << door.pos.show() << endl;

        } else if (reply == "wall") {

        } else { 
            //cout << "received something else, exiting program" << endl;
            return; 
        }
    }

    explore(maze, next, current);
}

int main() {
    Maze maze(200, 200);
    Pos start(99, 99);

    explore(maze, start, start);
    return 0;
}
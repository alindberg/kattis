#include <iostream>
#include <vector>
#include <string>
#include <unordered_map>
#include <algorithm>
#include <queue>

using namespace std;

int N = 5;
string finalboard = "111110111100 110000100000";

typedef pair<string, int> config;

#define x first
#define y second

vector<int> adjacent[25];
unordered_map<string, int> dist;

void bfs() {

    queue<config> Q;
    config start(finalboard, 12);
    Q.push(start);

    dist[finalboard] = 0; 

    while (!Q.empty()) {

        config cfg = Q.front();
        Q.pop();

        int curdist = dist[cfg.first];

        if (curdist == 10) break;

        int i = cfg.second;

        for (int j : adjacent[i]) {

            string nextboard = cfg.first;
            swap(nextboard[i], nextboard[j]);

            if (!dist.count(nextboard)) {
                Q.push(config(nextboard, j));
                dist[nextboard] = curdist + 1;
            }
            
        }
    }

}

int to_index(int x, int y) {
    return x + y * N;
}

vector<int> neighbours(int x, int y) {
    pair<int, int> pairs[8] = {
        make_pair(x - 1, y + 2),
        make_pair(x + 1, y + 2),
        make_pair(x - 2, y + 1),
        make_pair(x + 2, y + 1),
        make_pair(x - 2, y - 1),
        make_pair(x + 2, y - 1),
        make_pair(x - 1, y - 2),
        make_pair(x + 1, y - 2),
    };

    vector<int> moves;
    for (auto p : pairs) {

        if (p.x >= 0 && p.x < N &&
                p.y >= 0  && p.y < N) {
            moves.push_back(to_index(p.x, p.y));
        }
    }

    return moves;
}

void create_map() {
    int index = 0;
    for (int y = 0; y < N; y++) {
        for (int x = 0; x < N; x++) {
            adjacent[index++] = neighbours(x, y);
        }
    }
}

int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)
    
    int t;
    cin >> t; 
    cin.ignore();

    create_map();

    bfs();

    for (int i = 0; i < t; i++) {

        string board;

        for (int y = 0; y < N; y++) {
            string line;
            getline(cin, line);
            board.append(line);
        }

        if (dist.count(board)) {
            cout << "Solvable in " << dist[board] << " move(s).\n";
        } else {
            cout << "Unsolvable in less than 11 move(s).\n";
        }
        
    }
    

    return 0;
}


import Control.Monad (replicateM)

import Data.Set (Set)
import qualified Data.Set as Set

import Data.Map (Map)
import qualified Data.Map as Map

main = do
    line <- getLine
    let [n, k] = words line
    let n' = read n :: Int

    lines <- getLines n'

    let x = Set.empty
    let r = Set.empty

    let p = Set.fromList [0..n'-1]

    let graph = create_graph n' lines

    let a = bron r p x graph (read k)

    putStrLn $ show a


bron :: Set Int -> Set Int -> Set Int -> Map Int (Set Int) -> Int -> Int
bron r p x g k | size r == k = k
               | empty p && empty x = size r
               | otherwise = 
                
                --for each vertex in p, call bron, 
               --then modify P and X for next iteration

create_graph :: Int -> [String] -> Map Int (Set Int)
create_graph n xs = Map.fromList $ zip [0..n-1] zs
    where zs = map create_set xs

create_set :: String -> Set Int
create_set xs = Set.fromList . map read . tail . words $ xs


getLines :: Int -> IO [String]
getLines n = replicateM n getLine


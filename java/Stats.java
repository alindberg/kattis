package kattis;

import java.util.Scanner;

public class Stats {

	public static void main(String[] args) {
		
		int kase = 1;
		
		Scanner sc = new Scanner(System.in);
		
		
		while(sc.hasNextLine()) {
			int min = Integer.MAX_VALUE;
			int max = Integer.MIN_VALUE;
			
			String line = sc.nextLine();
			Scanner lineReader = new Scanner(line);
			lineReader.nextInt();
			
			while(lineReader.hasNextInt()) {
				int k = lineReader.nextInt();
				if(k < min) min = k;
				if(k > max) max = k;
			}

			int range = max - min;
			System.out.println("Case " + (kase++) + ": " + min + " " + max + " " + range);
			lineReader.close();
		}
		
		sc.close();
	}

}

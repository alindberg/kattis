#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
#include "Graph.h"

using namespace std;

class DFSColor {

    Graph graph;

    public:
    vector<bool> visited, color;
    bool isBipartite = true;

    DFSColor(Graph g): 
        visited(vector<bool>(g.vertices(), false)),
        color(vector<bool>(g.vertices(), false)),
        graph(g) {

        for(int v = 0; v < g.vertices(); v++) {
            if(!visited[v]) {
                explore(v);
            }
        }
    }

    void explore(int v) {
        visited[v] = true;

        for (int u : graph.adjacent(v)) {
            if(!visited[u]) {
                color[u] = !color[v];
                explore(u);
            } else if(color[v] == color[u]) {
                isBipartite = false; //could stop
            }
        }
    }
};

int main() {

    int n;
    cin >> n;
    unordered_map<string, int> map;

    cin.ignore(); 

    for(int i = 0; i < n; i++) {
        string item;
        getline(cin, item);
        map.insert({item, i});
    }

    Graph g(n);

    int p;
    cin >> p;

    for(int i = 0; i < p; i++) {
        string w1, w2;
        cin >> w1 >> w2;
        
        int u = map[w1];
        int v = map[w2];

        g.addEdge(u, v);
    }

    DFSColor dfs(g);

    vector<string> walter, jesse;

    if(!dfs.isBipartite) {
        cout << "impossible" << endl;
    } else {

        for (pair<string, int> p : map) {
            if (dfs.color[p.second]) {
                walter.push_back(p.first);
            } else {
                jesse.push_back(p.first);
            }
        }

        for (string w : walter) {
            cout << w << " ";
        }
        cout << endl;

        for(string j : jesse) {
            cout << j << " ";
        }
        cout << endl;


    }


    return 0;
}


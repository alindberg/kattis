import System.IO

main = do
    let initInterval = (0, 1001) 
    solve initInterval

solve :: (Int, Int) -> IO ()
solve (lower, higher) = do
    let mid = lower + quot (higher - lower) 2
    (putStrLn . show) mid
    hFlush stdout
    line <- getLine
    case line of 
        "correct" -> return ()
        "lower" -> solve (lower, mid)
        "higher" -> solve (mid, higher)
package util;

public class Timer {
	
	private Timer() {}
	
	private static long startTime, stopTime;
	
	public static void start() {
		startTime = System.nanoTime();
	}
	
	public static void stop() {
		stopTime = System.nanoTime();
	}
	
	public static double getSecondsElapsed() {
		double secondsElapsed = (stopTime - startTime) / 1000000000.0;
		startTime = 0;
		stopTime = 0;
		return secondsElapsed;
	}
	
	
}


package kattis;

import io.Kattio;

public class Baloni {

	public Baloni() {

		Kattio io = new Kattio(System.in, System.out);

		io.getInt(); 
		
		int[] ms = new int[1000002]; 
		int sf = 0; 

		while (io.hasMoreTokens()) { 
			int h = io.getInt(); 
			ms[h]++; 
			if(ms[++h] > 0) {
				ms[h]--;
			} else {
				sf++;
			}
		}
		
	
		io.println(sf);
		
		io.close();
	}

    
    public static void main(String[] args) {
    	new Baloni();
    }
}


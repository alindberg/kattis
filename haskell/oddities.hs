
main = do
    contents <- getContents
    let answers = map solve (parse contents)
    mapM_ putStrLn answers

solve :: Int -> String
solve x = show x ++ " is " ++ if even x then "even" else "odd"

parse :: String -> [Int]
parse = map read . tail . lines

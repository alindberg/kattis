package kattis;

import java.util.Arrays;

import io.Kattio;

public class CD {
	
	
	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		
		while(true) {
			int n = io.getInt(); //cd's owned by jack
			int m = io.getInt(); //cd's owned by jill
			
			if(n == 0 && m == 0) break;
			
			int[] jacks = new int[n];
			
			for(int i = 0; i < n; i++) {
				jacks[i] = io.getInt();
			}
			
			int count = 0;

			for(int i = 0; i < m; i++) {
				if(Arrays.binarySearch(jacks, io.getInt()) >= 0) count++;
			}
			
			io.println(count);
			
		}

		io.close();
	}

}

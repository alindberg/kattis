{-
By observation:

k = 1 (0, 1) has 2 substrings not containing 11.
k = 2 (00, 01, 10, 11) has 3 substrings not containing 11.

Then, for k >= 3: I imagine splitting the 2^k strings into two halves:

For the half that starts with 0, we have the same amount of
substrings not including 11 as the previous term.

For the other half that starts with 1, the amount
of new substrings not including 11 are the same as the substrings
counted with in the second previous term.
-}

main = do
    contents <- getContents
    let (_:cases) = map read (words contents) 
    
    let f = anti 10000 -- generate 10,000 terms
    mapM_ putStrLn $ map show [f x `mod` (10^9 + 7) | x <- cases]



anti :: Int -> Int -> Integer
anti _ 0 = 0
anti terms n = fst $ (take terms anti') !! (n - 1)
    where anti' = iterate (\(x, y) -> (y, x + y)) (2, 3)
    -- iterate f a = a : iterate f (f a)
    -- iterate (\(x, y) -> (y, x + y)) (2, 3) = 
    -- (3, 5) : iterate f (3, 5)
    -- (5, 8) : iterate .......


import Data.Set

main = do
    n <- getLine
    weights <- getLine
    putStrLn . show $ solve (read n) (map read (words weights))

solve :: Int -> [Int] -> Int
solve n ws = 2^(n - 1) * sum ws - sum subs
    where ps = (powerSet . fromList . filter (< 200)) ws
          subs = filter (< 200) $ map sum ps


{-
powerset :: [a] -> [[a]]
powerset [] = [[]]
powerset (x:xs) = [x:ps | ps <- powerset xs] ++ powerset xs
-}
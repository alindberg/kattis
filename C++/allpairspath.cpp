#include <iostream>
#include <vector>
#include <climits>

using namespace std;

typedef vector<int> vi;
typedef vector<vi> vvi;

#define INF 1000000

void floyd(vvi &A) {

    int n = A.size();

    for (int k = 0; k < n; k++) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
        
                int w = A[i][k] + A[k][j];
                if (A[i][k] < INF && A[k][j] < INF && w < A[i][j]) {
                    
                    A[i][j] = w;
                } 
            }
        }
    }

    //mark all that are in negative cycles... 
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            for (int k = 0; k < n && A[i][j] != -INF; k++) {
                if (A[i][k] != INF && A[k][j] != INF && A[k][k] < 0) {
                    A[i][j] = -INF;
                }
            }
        }
    }

}

int main() {
    ios::sync_with_stdio(false); 
    cin.tie(NULL); 
    cout.tie(NULL);

    int n, m, q, u, v, w;
    cin >> n >> m >> q;

    while (n != 0) {
        
        vvi dist(n, vector<int>(n, INF));

        for (int i = 0; i < n; i++) {
            dist[i][i] = 0;
        }

        for (int i = 0; i < m; i++) {
            cin >> u >> v >> w;
  
            dist[u][v] = min(w, dist[u][v]); //same edge could occur multiple times..
        }

        floyd(dist);

        for (int i = 0; i < q; i++) {
            cin >> u >> v;

            if (dist[u][v] == INF) {
                cout << "Impossible\n";
            } else if (dist[u][v] == -INF) {
                cout << "-Infinity\n";
            } else {
                cout << dist[u][v] << "\n";
            }
        }

        cin >> n >> m >> q;
        cout << "\n";
    }

    return 0;
}
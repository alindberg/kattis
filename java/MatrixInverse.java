package kattis;

import io.Kattio;

public class MatrixInverse {
	public static void main(String[] args) {
		int kase = 1;
		Kattio io = new Kattio(System.in, System.out);
		
		while(io.hasMoreTokens()) {
			int a = io.getInt();
			int b = io.getInt();
			int c = io.getInt();
			int d = io.getInt();
			int[][] A = {{a, b}, {c, d}};
			System.out.println("Case " + (kase++) + ":");
			printMatrix(inverse(A));
		}

		io.close();
	}

	static int[][] inverse(int[][] A) {
		int det = 1 / (A[0][0] * A[1][1] - A[0][1] * A[1][0]);
		return new int[][] { { det * A[1][1], -det * A[0][1] }, { -det * A[1][0], det * A[0][0] } };
	}
	
	static void printMatrix(int[][] A) {
		System.out.println(A[0][0] + " " + A[0][1]);
		System.out.println(A[1][0] + " " + A[1][1]);
	}

}

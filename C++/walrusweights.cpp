#include <iostream>

using namespace std;

int knap(int *weights, int n) {
    
    int W = 1000;
    int CAP = 2001;

    int **K = new int*[n + 1];
    for (int i = 0; i <= n; i++) {
        K[i] = new int[CAP]();
    }


   for(int j = 1; j <= n; j++) {
       int wj = weights[j-1];

       for(int w = 1; w < CAP; w++) {
           if(wj > w) {
               K[j][w] = K[j-1][w];
           } else {
               K[j][w] = max(K[j-1][w], K[j-1][w-wj] + wj);
           }
       }
   }

    int bestsum = 0;
    int bestdist = W + 1;

    for (int i = 0; i < CAP; i++) {
        int d = abs(K[n][i] - W);
        if (d < bestdist) {
            bestdist = d;
            bestsum = K[n][i];
        } else if (d == bestdist) {
            bestsum = max(bestsum, K[n][i]);
        }
    }
    
    return bestsum;
}


int main() {
    std::ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    std::cin.tie(NULL); // Speedup (do not mix with scanf/prinf)

    int n;
    cin >> n;
    int weights[n];
    for (int i = 0; i < n; i++) {
        cin >> weights[i];
    }

    cout << knap(weights, n) << endl;

    return 0;
}
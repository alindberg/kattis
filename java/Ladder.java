package kattis;

import io.Kattio;

public class Ladder {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		int h = io.getInt();
		int v = io.getInt();
		
		int l = (int) Math.ceil(h / Math.sin(v* (Math.PI / 180)));
		
		System.out.println(l);
		io.close();
	}

}

package kattis;

import io.Kattio;

public class NumberOnTree {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);

		int h = io.getInt();

		long root = (long)Math.pow(2, h + 1);
		
		long i = 1;
		if(io.hasMoreTokens()) {
			for(char c : io.getWord().toCharArray()) {
				i *= 2;
				if(c == 'R') {
					i++;
				}
			}
		}
		
		io.println(root - i);
		io.close();
	}
	


}

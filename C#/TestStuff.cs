using System;
using Graph;

class TestStuff {

    public static void Main(string[] args) {

        Graph undirected = new Graph(3);
        undirected.AddEdge(0, 1);
        undirected.AddEdge(0, 2);
        undirected.AddEdge(1, 2);
        Console.WriteLine(undirected);

        Graph directed = new Graph(3, directed=true);
        directed.AddEdge(0, 1);
        directed.AddEdge(0, 2);
        directed.AddEdge(1, 2);
        Console.WriteLine(directed);

    }

}
#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include <unordered_map>
#include <algorithm>

using namespace std;

typedef vector<int> vi;
typedef vector<bool> vb;

typedef pair<int, int> pii;
typedef vector<pii> vii;

#define x first
#define y second

int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)
    cout.tie(NULL);

    int t, n, head, tail, result;
    cin >> t;
    unordered_map<int, int> imap;

    for (int i = 0; i < t; i++) {
        cin >> n;

        vector<int> elems(n);
        for (int j = 0; j < n; j++) {
            cin >> elems[j];
        }

        result = 0;
        imap.clear();
        head = 0;
        for (int tail = 0; tail < n; tail++) {

            //while the tail element exists, move the head. ~~sliding window.
            while (imap.find(elems[tail]) != imap.end()) {
                imap.erase(elems[head++]);
            }
            imap[elems[tail]];
            if (imap.size() > result) result = imap.size();
        }

        cout << result << endl;
    }

    return 0;
}
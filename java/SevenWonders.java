package kattis;

import io.Kattio;

public class SevenWonders {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		
		String word = io.getWord();

		int t = 0; 
		int c = 0;
		int g = 0;
		for(int i = 0; i < word.length(); i++) {
			char z = word.charAt(i);
			if(z == 'T') t++;
			else if(z == 'C') c++;
			else g++;
		}
		
		int sum = t * t + c * c + g * g;
		sum += 7 * min(t, c, g);
		io.println(sum);
		io.close();
	}

	
	static int min(int a, int b, int c) {
		int[] nums = {a, b, c};
		int min = Integer.MAX_VALUE;
		for(int k : nums) {
			if(k < min) min = k;
		}
		return min;
	}
}

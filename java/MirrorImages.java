package kattis;

import io.Kattio;

public class MirrorImages {
	
	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		
		int t = io.getInt(); // Test cases .. . .
		
		for(int i = 0; i < t; i++) {
			int r = io.getInt();
			int c = io.getInt();
			boolean[][] maze = new boolean[r][c];
			for(int y = 0; y < r; y++) {
				String line = io.getWord();
				for(int x = 0; x < c; x++) {
					if(line.charAt(x) == '*') {
						maze[y][x] = true;
					}
				}
			}
			System.out.println("Test " + (i + 1));
			show(rotate(maze));
		}
		
		
		io.close();
	}
	
	
	private static void show(boolean[][] maze) {
		for(int y = 0; y < maze.length; y++) {
			for(int x = 0; x < maze[y].length; x++) {
				if(maze[y][x])
					System.out.print('*');
				else 
					System.out.print('.');
			}
			System.out.println();
		}
	}
	
	/*
	 
	  
	  ***.
	  **..
	  ....
	  ....
	  
	  ....
	  ....
	  ..**
	  .***
	  
	  [0][0] -> [3][3]
	  [0][1] -> [3][2]
	  [0][2] -> [3][1]
	  [0][3] -> [3][0]
	  
	  [1][0] -> [2][3]
	  
	  
	 * 
	 * */
	
	private static boolean[][] rotate(boolean[][] maze) {
		int r = maze.length;
		int c = maze[0].length;
		boolean[][] nm = new boolean[r][c];
		for(int y = 0; y < r; y++) {
			for(int x = 0; x < c; x++) {
				nm[r - 1 - y][c - 1 - x] = maze[y][x];
			}
		}
		
		return nm;
	}
	


}

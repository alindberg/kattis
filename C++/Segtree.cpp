#include <iostream>
#include <iterator>
#include "Segtree.h"

Segtree::Segtree(int *a, int n) {
    N = 2;
    while (2 * n + 3 > N) N *= 2;

    offs = 2;
    while (n + 2 > offs) offs *= 2;

    A = new int[N](); 

    //copy(begin(src), end(src), begin(dest))
    std::copy(a, a + n, A + offs + 1);

    for (int i = offs - 1; i > 0; i--) {
        A[i] = A[2 * i] + A[2 * i + 1];
    }
}


//Return a[i] + ... + a[j].
int Segtree::getSum(int i, int j) {
    int L = i + offs - 1;
    int R = j + offs + 1;
    int ans = 0;

    while(true) {
        bool Lright = L % 2 == 0;
        bool Rleft = R % 2 == 1;
        L /= 2;
        R /= 2;
        if (L == R) break;
        if (Lright) ans += A[2 * L + 1];
        if (Rleft) ans += A[2 * R];
    }

    return ans;
}

//Point update
void Segtree::update(int i, int v) {
   int pos = i + offs;
   int diff = v - A[pos];
   do {
       A[pos] += diff;
       pos /= 2;
   } while (pos > 0);
}

void Segtree::show() {
    std::cout << "Segtree with N = " << N << ", offs = " << offs << ", content:" << std::endl;
    
    for (int i = 0; i < N; i++) {
        std::cout << A[i] << " ";
    }
    std::cout << std::endl;
}

int main() {
    int a[14] = {5, 2, 3, 1, 7, 2, 4, 6, 3, 9, 9, 2, 1, 5};
    Segtree segtree(a, 14);

    segtree.update(7, 100);
    
    std::cout << segtree.getSum(4, 7) << std::endl;

    segtree.show();

    return 0;
}
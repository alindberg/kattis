#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include <queue>
#include <algorithm>

using namespace std;

typedef vector<int> vi;
typedef vector<bool> vb;

typedef pair<int, int> pii;
typedef vector<pii> vii;

#define x first
#define y second

int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)
    cout.tie(NULL);

    int n, m, u, v;
    cin >> n >> m;

    while (n > 0) {
        vector<vector<int>> graph(n);

        for (int i = 0; i < m; i++) {
            cin >> u >> v;
            graph[u].push_back(v);
        }

        int odd = 0;
        int s, f;
        for (int u = 0; u < n; u++) {

            if (graph[u].size() % 2 != 0) {

                if (odd == 0) 
                    s = u;
                else if (odd == 1) 
                    f = u;

                odd++;
            }
        }

        if (odd != 2) cout << "Impossible" << endl;

        vector<bool> visited(n);
        queue<int> Q;


        //connected AND each has even degree, except two.
        cin >> n >> m;
    }

    return 0;
}
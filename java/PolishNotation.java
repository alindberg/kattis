package kattis;

import java.util.ArrayList;
import java.util.Scanner;

public class PolishNotation {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		ArrayList<String> output = new ArrayList<>();
		
		while (sc.hasNextLine()) {

			String[] spl = sc.nextLine().split(" ");
			if(spl.length == 1 && spl[0].length() == 0) break;
			
			int size = spl.length;
			int i = size - 1;
			while(i >= 0 && size > 1) {
				if(isOperator(spl[i]) && isNum(spl[i+1]) && isNum(spl[i + 2])) { 
					int v1 = Integer.parseInt(spl[i + 1]);
					int v2 = Integer.parseInt(spl[i + 2]);
					String op = spl[i];
					spl[i] = calculate(op, v1, v2);
					int j = i + 1;
					for(int s = i + 3; s < size; s++) {
						spl[j++] = spl[s];
					}
					size -= 2;
				} 
				i--;
			}
			
			output.add(unwords(spl, size));

		}
		for(int i = 0; i < output.size(); i++) {
			System.out.println("Case " + ((i + 1) + ": " + output.get(i)));
		}
		sc.close();
	}

	static String unwords(String[] arr, int s) {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < s; i++) {
			sb.append(arr[i] + " ");
		}
		
		return sb.toString().trim();
	}


	static String calculate(String op, int a, int b) {
		switch (op) {
		case "+":
			return String.valueOf(a + b);
		case "-":
			return String.valueOf(a - b);
		default:
			return String.valueOf(a * b);
		}
	}
	
	private static boolean isNum(String s) {
		return s.length() > 1 || Character.isDigit(s.charAt(0));
	}

	static boolean isOperator(String s) {
		if(s.length() != 1) return false;
		char k = s.charAt(0);
		return k == '+' || k == '-' || k == '*';
	}

}

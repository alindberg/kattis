main = interact (show . solve . words)

solve :: [String] -> Int
solve (r1:s:[]) = let r1' = read r1 :: Int
                      s' = read s :: Int
                  in 2*s' - r1'
solve _ = 0
    

type Stars = Int
type Rank = Int
type Streak = Int

data Player = Legend | Regular Rank Stars Streak

instance Show Player where
    show Legend = "Legend"
    show (Regular rank _ _) = show rank

main = do
    history <- getLine
    let player = foldl step (Regular 25 0 0) history
    putStrLn (show player)

step :: Player -> Char -> Player
step p c = updaterank (updatestar c p)

updatestar :: Char -> Player -> Player
updatestar _ Legend = Legend

updatestar 'W' (Regular r s w) = Regular r (s + bonus) (w + 1)
    where bonus = if r > 5 && streak (w + 1) then 2 else 1

updatestar 'L' (Regular r s _) = Regular r (s - punishment) 0
    where punishment = if r > 20 then 0 else 1

updatestar x (Regular r s w) = error $ show x ++ ", " ++ show r ++ ", " ++ show s ++ ", " ++ show w


updaterank :: Player -> Player
updaterank Legend = Legend
updaterank (Regular r s w) | s == -1 && r < 20 = Regular (r + 1) (req (r + 1) - 1) 0
                           | s == -1 = Regular r 0 0
                           | s > req r && r == 1 = Legend
                           | s == req r + 1 && streak w = Regular (r - 1) 1 w
                           | s > req r = Regular (r - 1) (s - req r) w
                           | otherwise = Regular r s w


streak :: Streak -> Bool
streak = (>=3)

req :: Rank -> Stars
req r | r > 20 = 2
      | r > 15 = 3
      | r > 10 = 4
      | otherwise = 5
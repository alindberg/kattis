package kattis;

import io.Kattio;

public class Pet {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		
		int sum = 0;
		int j = 0;
		for(int i = 0; i < 5; i++) {
			int g1 = io.getInt();
			int g2 = io.getInt();
			int g3 = io.getInt();
			int g4 = io.getInt();
			int s = g1 + g2 + g3 + g4;
			if(s > sum) { 
				sum = s;
				j = i;
			}
			
		}
		
		System.out.println((j + 1) + " " + sum);
		io.close();
	}

}

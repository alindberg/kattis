using System;

public class Problem {

    static int Quadrant(int x, int y) {

        if (x >= 0 && y >= 0) {
            return 1;
        } else if (x <= 0 && y > 0) {
            return 2;
        } else if (x <= 0 && y < 0) {
            return 3;
        } else {
            return 4;
        }
    }

    public static void Main() {


        int x = Convert.ToInt32(Console.ReadLine());
        int y = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine(Quadrant(x, y));
    }

}


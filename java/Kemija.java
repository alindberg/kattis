package kattis;

import io.Kattio;

public class Kemija {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		
		while(io.hasMoreTokens()) {
			io.print(decode(io.getWord()) + " ");
		}
		
		io.close();
	}

	static String decode(String xs) {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < xs.length(); i++) {
			sb.append(xs.charAt(i));
			if(isVowel(xs.charAt(i))) {
				i+=2;
			}
		}
			
		return sb.toString();
	}
	
	static boolean isVowel(char c) {
		return "aeiou".contains(String.valueOf(c));
	}
}

package kattis;

import io.Kattio;

public class Herman {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		
		int r = io.getInt();
		io.println(Math.PI * r * r);
		io.println(2*r*r);
		
		io.close();
	}
	
	static double dist(double x1, double y1, double x2, double y2) {
		return Math.abs(x1 - x2) + Math.abs(y1 - y2);
	}

}

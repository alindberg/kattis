
type Case = (Int, [Int])

main = do
    contents <- getContents
    let (_:contents') = words contents --ignore the number of cases part
    solve contents'
    

solve :: [String] -> IO ()
solve [] = return () 
solve xs = do
    let (c, rest) = parse xs
    let (earliest, latest) = compute c

    putStrLn $ show earliest ++ " " ++ show latest
    
    solve rest 

compute :: Case -> (Int, Int)
compute (len, positions) =
    let mid = div len 2
        distances = map (\x -> (abs (x - mid), x)) positions

        midmost = snd (minimum distances)
        earliest = min (len - midmost) midmost

        edgiest = snd (maximum distances)
        latest = max (len - edgiest) edgiest

    in (earliest, latest)


parse :: [String] -> (Case, [String])
parse (len:a:xs) = 
    let ants = read a
        positions = (map read . take ants) xs
        rest = drop ants xs
    in ((read len, positions), rest)

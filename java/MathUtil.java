package util;

public class MathUtil {

	/**
	 * 
	 * @param n
	 * @return 1^2 + 2^2 + 3^2 + ... + (n-1)^2 + n^2 = (n/6) * (n+1) * (2n+1)
	 */
	public static int sumTermsSquared(int n) {
		return (n / 6) * (n + 1) * (2 * n + 1);
	}

	/**
	 * hmmm ...?
	 * 
	 * @param start
	 * @param end
	 * @param n
	 *            number of terms
	 * @return start + (start + 1) + (start + 2) ... end = (n / 2) * (start +
	 *         end)
	 */
	public static long sum(int start, int end, int n) {
		return (int) ((n / 2.0) * (start + end));
	}
	
	/**
	 * 
	 * @param n
	 * @return 1 + 2 + ... + n = (n / 2) * (n + 1)
	 */
	public static double sum(int n) {
		return (n / 2.0) * (1 + n);
	}

	public static boolean isPrime(int n) {
		if (n <= 1 || n > 2 && n % 2 == 0)
			return false;

		for (int i = 3; i * i <= n; i += 2) {
			if (n % i == 0)
				return false;
		}

		return true;
	}

	// terrible
	/**
	 * 
	 * @param total
	 * @param choose
	 * @return n choose k (?)
	 */
	public static long choose(long total, long choose) {
		if (total < choose)
			return 0;
		if (choose == 0 || choose == total)
			return 1;
		return choose(total - 1, choose - 1) + choose(total - 1, choose);
	}
}

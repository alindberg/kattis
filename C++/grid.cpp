#include <iostream>
#include <string>
#include <vector>
#include <queue>
#include <climits>

using namespace std;

typedef vector<int> vi;
typedef vector<bool> vb;
typedef vector<vb> vvb;
typedef vector<vi> vvi;
typedef pair<int, int> pii;

#define x first
#define y second

vector<pii> neighborsOf(int x, int y, int k, int n, int m) {
    pii candidates[] = {pii(x, y - k), pii(x - k, y), pii(x + k, y), pii(x, y + k)};

    vector<pii> neighbors;
    for (auto &pos: candidates) {

        if (pos.y >= 0 && pos.y < n && pos.x >= 0 && pos.x < m) {
            neighbors.push_back(pos);
        }
    }

    return neighbors;
}

int solve(vvi &grid, int n, int m) {

    vvb visited (n, vb(m, false));
    vvi moves(n, vi(m, INT_MAX));
    moves[0][0] = 0;
    visited[0][0] = true;

    queue<pii> Q;
    Q.push(pii(0, 0));

    while (!Q.empty()) {
        pii u = Q.front();
        Q.pop();

        visited[u.y][u.x] = true;
        int k = grid[u.y][u.x];

        for (auto &v : neighborsOf(u.x, u.y, k, n, m)) {

            if (moves[v.y][v.x] > moves[u.y][u.x] + 1) {
                moves[v.y][v.x] = moves[u.y][u.x] + 1;
            }
            if (!visited[v.y][v.x]) {
                Q.push(v);
                visited[v.y][v.x] = true;
            }
        }
    }

    return visited[n-1][m-1] ? moves[n-1][m-1] : -1;
}

int main() {

    int n, m;
    cin >> n >> m;

    vvi grid(n, vi(m));

    for (int y = 0; y < n; y++) {
        string line;
        cin >> line;
        for (int x = 0; x < m; x++) {
            grid[y][x] = line[x] - '0';
        }
    }

    cout << solve(grid, n, m) << endl;

    return 0;
}

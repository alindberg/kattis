#include <iostream>
#include <vector>
#include "Digraph.h"
#include <algorithm>

using namespace std;

typedef pair<int, int> post_vertex; 

class DFS {
    int time = 0;

    public:
    int cc = 0;
    vector<bool> visited;
    vector<int> comp;
    vector<post_vertex> posts;

    DFS(Digraph &graph):
        visited(vector<bool>(graph.vertices(), false)),
        posts(vector<post_vertex>(graph.vertices())),
        comp(vector<int>(graph.vertices())) {

        for (int u = 0; u < graph.vertices(); u++) {
            if(!visited[u]) {
                explore(u, graph);
                cc++;
            }
        }
    }

    //Visit vertices in the order of descending post values.
    DFS(Digraph &graph, vector<post_vertex> &sortedposts):
        visited(vector<bool>(graph.vertices(), false)),
        posts(vector<post_vertex>(graph.vertices())),
        comp(vector<int>(graph.vertices())) {

        for (post_vertex pv : sortedposts) {
            int u = pv.second;
            if(!visited[u]) {
                explore(u, graph);
                cc++;
            }
        }
    }

    void explore(int u, Digraph &graph) {
        visited[u] = true;
        comp[u] = cc;
        time++;

        for (int v : graph.adjacent(u)) {
            if (!visited[v]) {
                explore(v, graph);
            }
        }

        posts[u] = make_pair(time, u);
        time++;
    }

};


int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)

    int t;
    cin >> t;
    for (int i = 0; i < t; i++) {
        
        int n, m;
        cin >> n >> m;

        Digraph graph(n);
        Digraph reversed(n);
        
        for (int i = 0; i < m; i++) {
            int s1, s2;
            cin >> s1 >> s2;
            graph.addEdge(s1 - 1, s2 - 1);
            reversed.addEdge(s2 - 1, s1 - 1);
        }

        //Run DFS on the reversed graph, sort POST descendingly.
        DFS dfs_rev(reversed);
        sort(dfs_rev.posts.rbegin(), dfs_rev.posts.rend());

        //Run DFS on the original graph to find connected components.
        DFS dfs(graph, dfs_rev.posts);

        if (dfs.cc == 1) {
            cout << 0 << endl;
            continue;
        }

        vector<int> indegrees(dfs.cc, 0);
        vector<int> outdegrees(dfs.cc, 0);

        int sources = dfs.cc;
        int sinks = dfs.cc;

        for (int u = 0; u < graph.vertices(); u++) {

            for (int v : graph.adjacent(u)) {
                //v has an edge to another component.
                if (dfs.comp[u] != dfs.comp[v]) {
                    if(outdegrees[dfs.comp[u]] == 0) {
                        sources--;
                    }
                    
                    if(indegrees[dfs.comp[v]] == 0) {
                        sinks--;
                    }

                    outdegrees[dfs.comp[u]]++;
                    indegrees[dfs.comp[v]]++;
                }
            }
        }

        cout << max(sources, sinks) << endl;
    }

    return 0;
}

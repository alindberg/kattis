main = do
    nums <- getLine
  --  let ints = (map read . words) nums :: [Int]
    case (map read . words) nums :: [Int] of 
        x:y:n:[] -> do
            --let fs = [fizz a x y | a <- [1 .. n]]
            (mapM_ putStrLn) [fizz a x y | a <- [1 .. n]]
        _ -> putStrLn "error"

    
    
fizz :: Int -> Int -> Int -> String
fizz a x y | mod a x == 0 && mod a y == 0 = "FizzBuzz"
       | mod a x == 0 = "Fizz" 
       | mod a y == 0 = "Buzz"
       | otherwise = show a
main = do
    contents <- getContents
    let ds = (map read . words) contents
    mapM_ putStrLn (solve ds)
    
solve :: [Int] -> [String]
solve [] = []
solve (0:0:xs) = []
solve (n:d:xs) = (a ++ " " ++ b ++ " / " ++ c) : solve xs
    where a = show $ div n d
          b = show $ mod n d
          c = show d
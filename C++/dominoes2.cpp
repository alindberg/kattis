#include <iostream>
#include <vector>
#include <string>
#include <queue>
#include <cmath>
#include <algorithm>

using namespace std;

typedef vector<int> vi;
typedef vector<bool> vb;

typedef pair<int, int> pii;
typedef vector<pii> vii;

int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)
    cout.tie(NULL);

    int t, n, m, l, x, y, z;
    
    cin >> t;
    for (int i = 0; i < t; i++) {

        cin >> n >> m >> l;
        //n domino tiles

        vector<vector<int>> graph(n);
        vector<bool> knocked(n, false);

        for (int j = 0; j < m; j++) {
            cin >> x >> y;  //x will cause y to fall
            graph[x-1].push_back(y-1);
        }

        for (int j = 0; j < l; j++) {
            cin >> z; //z is knocked over by hand.
            z--;
            queue<int> Q;
            Q.push(z);
            while (!Q.empty()) {
                int u = Q.front();
                Q.pop();
                knocked[u] = true;
                for (int v : graph[u]) {
                    if (!knocked[v]) {
                        knocked[v] = true;
                        Q.push(v);
                    }
                }
            }
        }

        int count = 0;
        for (bool b : knocked) {
            if (b) count ++;
        }

        cout << count << endl;
    }

    return 0;
}

main = interact (show . solve . words)


solve :: [String] -> Int
solve [] = 0
solve (x:xs) = 
    case x of 
        [c] -> 0 + solve xs 
        cs -> let a = (read . init) cs
                  b = (read . (\k -> [k]) . last) cs
              in a^b + solve xs
    
    
    
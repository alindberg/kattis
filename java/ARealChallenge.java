package kattis;

import Kattio;

public class ARealChallenge {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		
		io.print(4 * Math.sqrt(io.getLong()));
		
		io.close();
	}

}

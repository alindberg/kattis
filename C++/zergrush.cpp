#include <iostream>
#include <vector>
#include <string>
#include <queue>
#include <algorithm>

using namespace std;

#define inf 1000000000
#define x first
#define y second
#define dead 0
#define max_hp 35
#define base_attack 5

typedef pair<int, int> pii;


int mhdist(pii p0, pii p1) {
    return abs(p0.x - p1.x) + abs(p0.y - p1.y);
}


struct Player {
    int attack;
    int armor;
    vector<pii> pos;
    // Player(int attack, int armor): attack(attack), armor(armor) {}
};

struct Ling {
    bool player;
    int hp;
    int closest_dist;
    pii direction;

    Ling(bool player, int hp): player(player), hp(hp) {
        closest_dist = 1 << 31;
        direction = make_pair(0, 0);
    }

    Ling(): Ling(false, 0) {}
};

void show(vector<Ling> &board, int &N) {

    cout << "Result:" << endl;
    for (int i = 0; i < N; i++) {
        int row = i * N;
        for (int j = 0; j < N; j++) {
            if (board[row + j].hp == 0) {
                cout << ".";
            } else {  
                cout << (board[row + j].player ? 2 : 1);
            }
        }
        cout << endl;
    }
}


void run(vector<Ling> board, const int N, const Player &p0, const Player &p1, const int iterations) {

    for (int t = 0; t < iterations; t++) {

        /*
        1. Check distance between each point. Do attacks first. If distance is 1, set the direction. 
        2. If distance is 1, and the direction is already set, then compare the direction. Choose the right one. (N, NE, E, etc)
        
        */

        vector<pii> next_pos_p0, next_pos_p1;

        for (int i = 0; i < p0.pos.size(); i++) {

            int min_dist = inf;
            for (int j = 0; j < p1.pos.size(); j++) {

                int dist = mhdist(p0.pos[i], p1.pos[j]);
                
                if (dist == 1) {
                    //check directions. if not equal (0,0).. 
                } 
            }
        }

        vector<Ling> nextBoard(N * N);

        //set northernmost if norther than north
        //set westernmost if wester than west

        for (int y = 0; y < N; y++) {
            int row = y * N;
            for (int x = 0; x < N; x++) {

                if (board[row + x].hp > 0) {    

                    bool player = board[row + x].player;
                    
                    //Scan surrounding 8's. If attack, attack. Goes both ways. If not attack, collect. Clean up dead lings.

                    //do ling -= dmg_incurred. if opposite ling hp < 0, then its dead.

                    int attack_value = base_attack + player ? p0.attack : p0.attack;
                    int dmg_incurred = attack_value - player ? p0.armor : p0.armor;
                    // Then, do all attacks. all dead lings removed.
                    // Damage incurred = attack value minus armor upgrade of the player owning the attacked ling, i.e. (base_attack + px.attack) - py.armor 

                    // If opponent ling within 8 surrounding squares, attack it. 
                    // Else, if other player has more than 0 lings, move towards the closest one. If tie, pick clockwise, starting from north (then northeast, etc)

                    // Then do movement:

                    // If the next square is occupied by a zergling not moving, then don't move.
                    // Else, If more than one zergling move to the same square, then northernmost moves, other remain stationary. 
                        // If more than one northernmost, then the westernmost moves.



                    //After each turn, each alive ling regain one hit point 
                } 
            }

        }

        board = nextBoard;
    }
}

int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)

    int N;
    cin >> N;

    Player p0, p1;
    cin >> p0.attack >> p0.armor;
    cin >> p1.attack >> p1.armor;

    vector<Ling> board(N * N);

    for (int i = 0; i < N; i++) {
        string line;
        cin >> line;
        int row = i * N;

        for (int j = 0; j < N; j++) {
            
            if (line[j] == '.') {
                board[row + j] = Ling(false, dead);
            } else if (line[j] == '1') {
                board[row + j] = Ling(false, max_hp);
                p0.pos.push_back(pii(i, j));
            } else {
                board[row + j] = Ling(true, max_hp);
                p1.pos.push_back(pii(i, j));
            }
        }
    }

    int t;
    cin >> t;

    run(board, N, p0, p1, t);

    //place them on the board..

    show(board, N);

    return 0;
}
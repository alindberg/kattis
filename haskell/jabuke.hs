import Numeric (showFFloat)
import Control.Monad (replicateM)

type Vec2D = (Int, Int)
data Shape = Triangle Vec2D Vec2D Vec2D

main = do
    (a:b:c:t:_) <- getLines 4
    ts <- getLines (read t)
    let triangle = Triangle (readVec2D a) (readVec2D b) (readVec2D c)
    let trees = map readVec2D ts

    putStrLn $ format (area triangle) 1

    let ins = filter (inside triangle) trees
    putStrLn . show . length $ ins


inside :: Shape -> Vec2D -> Bool
inside (Triangle a b c) p = 
    right (vec a b) (vec a p) &&
    right (vec b c) (vec b p) && 
    right (vec c a) (vec c p) ||
    left (vec a b) (vec a p) &&
    left (vec b c) (vec b p) && 
    left (vec c a) (vec c p)

--v right of u
left :: Vec2D -> Vec2D -> Bool
left u v = dot u (rotateLeft v) <= 0

--v right of u
right :: Vec2D -> Vec2D -> Bool
right u v = dot u (rotateLeft v) >= 0

rotateLeft :: Vec2D -> Vec2D
rotateLeft (x, y) = (-y, x)

dot :: Vec2D -> Vec2D -> Double
dot (x1, y1) (x2, y2) = fromIntegral $ x1 * x2 + y1 * y2

vec :: Vec2D -> Vec2D -> Vec2D
vec (x1, y1) (x2, y2) = (x2 - x1, y2 - y1)

area :: Shape -> Double
area (Triangle (x1, y1) (x2, y2) (x3, y3)) = (1/2) * fromIntegral s 
    where s = abs $ x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)

readVec2D :: String -> Vec2D
readVec2D nums = (read x, read y)
    where [x, y] = words nums

format :: RealFloat a => a -> Int -> String
format x decimals = showFFloat (Just decimals) x ""

getLines :: Int -> IO [String]
getLines n = replicateM n getLine
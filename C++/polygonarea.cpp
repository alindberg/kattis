#include <iostream>
#include <vector>

using namespace std;

#define x first
#define y second

typedef pair<int, int> pii;
typedef vector<pii> Polygon;

int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)
    
    int n;
    cin >> n;

    while (n) {

        Polygon p(n);
        
        for (int i = 0; i < n; i++) {
            cin >> p[i].x >> p[i].y;
        }

        //Find area and whether CW or CCW
        int x1, x2, y1, y2;
        double area = 0;
        double c = 0;
        for (int i = 1; i < n; i++) {
            x1 = p[i-1].x;
            y1 = p[i-1].y;
            x2 = p[i].x;
            y2 = p[i].y;

            area += x1 * y2 - x2 * y1;
            c += (x2 - x1) * (y2 + y1);
        }

        area = 0.5 * (area + p[n-1].x * p[0].y - p[n-1].y * p[0].x);
        c += (p[0].x - p[n-1].x) * (p[0].y + p[n-1].y);

        printf("%s %0.1f\n", (c < 0 ? "CCW" : "CW"), abs(area));

        cin >> n;
    }

    return 0;
}

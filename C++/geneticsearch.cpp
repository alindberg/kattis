#include <iostream>
#include <vector>
#include <array>
#include <string>
#include <unordered_map>

using namespace std;

int count(string &s, string &l) {
    int c = 0;

    ///cout << "received s = " << s << ", l = " << l << endl;

    int j = l.find(s);
    while (j != string::npos) {
        c++;
        j = l.find(s, j + 1);
    }
    return c;
}

int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)
    
    string alphabet = "AGCT";

    string s;
    cin >> s;
    while (s[0] != '0') {
        string l; 
        cin >> l;
        
        int A = count(s, l);

        unordered_map<string, int> map;
       
        //removing one character
        int B = 0;
        for (int i = 0; i < s.size(); i++) {
            string copy = s;
            copy.erase(i, 1);
            if (!map.count(copy)) {
                B += count(copy, l);
                map[copy];
            }
        }


        //adding one character
        int C = 0; 
        for (int i = 0; i < alphabet.size(); i++) {
            for (int j = 0; j <= s.size(); j++) {
                string copy = s;
                copy.insert(j, 1, alphabet[i]);
                if (!map.count(copy)) {
                    C += count(copy, l);
                   // cout  << "copy = " << copy << endl;
                    map[copy];
                }
            }
        }

        cout << A << " " << B << " " << C << endl;

        cin >> s;
    }

    return 0;
}

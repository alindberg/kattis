package kattis;

import io.Kattio;

public class Zamka {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		
		int L = io.getInt();
		int D = io.getInt();
		int X = io.getInt();
		
		int N = L;
		while(sumOfDigits(N) != X ) {
			N++;
		}
		
		io.println(N);
		
		int M = D;
		while(sumOfDigits(M) != X) {
			M--;
		}
		
		io.println(M);
		
		io.close();
	}

	
	private static int sumOfDigits(int k) {
		int sum = 0;
		for(char c : String.valueOf(k).toCharArray()) {
			sum += Character.getNumericValue(c);
		}
		return sum;
	}
}

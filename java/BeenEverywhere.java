package kattis;

import java.util.HashSet;
import java.util.Set;

import io.Kattio;

public class BeenEverywhere {


	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		
		int t = io.getInt();
		for(int i = 0; i < t; i++) {
			int n = io.getInt();
			Set<String> s = new HashSet<>(n);
			for(int j = 0; j < n; j++) {
				s.add(io.getWord());
			}
			io.println(s.size());
		}
		
		io.close();
		
	}

}

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

typedef vector<int> vi;
typedef vector<bool> vb;

typedef pair<int, int> pii;
typedef vector<pii> vii;

#define x first
#define y second

bool ccw(pii &a, pii &b, pii &c) {
    return (b.x - a.x) * (c.y - a.y) - (c.x - a.x) * (b.y - a.y) > 0;
}

//assume P is sorted
vii convexhull(vii &points) {

    vii hull;
    hull.push_back(points[0]);
    hull.push_back(points[1]);

    for (int i = 2; i < points.size(); i++) {
        int n = hull.size();
        while (n >= 2 && !ccw(hull[n - 2], hull[n - 1], points[i])) {
           hull.pop_back();
           n = hull.size();
        }
        hull.push_back(points[i]);
    }

    return hull;
}


vii solve(vii &points) {

    sort(points.begin(), points.end());

    vii reversed = vii(points.rbegin(), points.rend());

    vii lower = convexhull(points);
    vii upper = convexhull(reversed);

    lower.pop_back();
    upper.pop_back();

    lower.insert(lower.end(), upper.begin(), upper.end());
    if (lower.size() == 2 && lower[0] == lower[1]) {
        lower.pop_back();
    }

    return lower;
}


int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)

    int n;
    cin >> n;

    while (n) {

        vii points(n);
        for (int i = 0; i < n; i++) {
            cin >> points[i].x >> points[i].y;
        }

        if (n < 2) {
            cout << 1 << endl;
            cout << points[0].x << " " << points[0].y << endl;

        } else {

            vii hull = solve(points);

            cout << hull.size() << endl;
            for (int i = 0; i < hull.size(); i++) {
                cout << hull[i].x << " " << hull[i].y << endl;
            }

        }

        cin >> n;
    }

    return 0;
}
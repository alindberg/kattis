
public class Graph {
	
	private final int V;
	private int E;
	private Bag<Integer>[] adj;
	
	public Graph(int V) {
		this.V = V;
		adj = new Bag[V];
		for(int i = 0; i < V; i++) {
			adj[i] = new Bag();
		}
	}

	public void addEdge(int v, int w) {
		adj[v].put(w);
		adj[w].put(v);
		E++;
	}
	
	public int V() {
		return V;
	}
	
	public int E() {
		return E;
	}
	
	public Iterable<Integer> adj(int v) {
		return adj[v];
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < V; i++) {
			sb.append("adj(" + i + ") = ");
			for(Integer z : adj(i)) {
				sb.append(z + " ");
			}
			
			sb.append("\n");
		}
		return sb.toString();
	}
	
}

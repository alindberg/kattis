using System.Collections;

namespace Structures {

    public class Graph {

        private readonly int Vertices { get; }
        private bool Directed { get; }
        private int Edges { get; }
        private ArrayList<int>[] adj;

        public Graph(int v, bool directed=false) {
            this.vertices = v;
            this.directed = directed;
        }

        public void AddEdge(int u, int v) {
            adj[u].Add(v);

            if (!directed) adj[v].Add(u);

            Edges++;
        }

        public ArrayList<int> Adjacent(int v) {
            return adj[v];
        }

        public override string ToString() {
            string g = "Graph g:\n";
            for (int i = 0; i < adj.Length; i++) {
                g += string.Join(", ", adj[i]) + "\n";
            }
            return g;
        }

    }

}
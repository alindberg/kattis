#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

//unfinished..


int search(pair<int, int> doll, vector<pair<int, int>> dolls) {

    if(dolls.size() == 0) {
        return -1;
    }

    //FIND A DOLL STRICLY LESS THAN ***DOLL***

    int lo = 0;
    int hi = dolls.size();
    int mid;
    while (hi - lo > 1) {
        mid = (lo + hi) / 2;

        if (dolls[mid] >= doll || doll.first == dolls[mid].first || doll.second == dolls[mid].second) {
            hi = mid;
        } else {
            lo = mid; //go right
        }
    }

    bool lt = dolls[lo].first < doll.first && dolls[lo].second < doll.second;
    return lt ? lo : -1;
}

int mindolls(vector<pair<int, int>> dolls) {

    sort(dolls.begin(), dolls.end());

    cout << "Sorted dolls: ";

    for (pair<int,int> p : dolls) {
        cout << "(" << p.first << ", " << p.second << "), ";  
    }

    cout << endl;
    
    int m = dolls.size();

    vector<pair<int, int>> reserve;

    for (int i = 0; i < dolls.size() - 1; i++) {
        if (dolls[i].first < dolls[i + 1].first && dolls[i].second < dolls[i + 1].second) {
            m--;

        } else {
            int index = search(dolls[i], reserve);

            cout << "looking for something to put inside (" << dolls[i].first << ", " << dolls[i].second << ") and found index = " << index << endl;
            
            if (index != -1) {
                m--;
                
                cout << "reserve contains:" << endl;
                for (pair<int, int> p : reserve) {
                    cout << "(" << p.first << ", " << p.second << "), ";
                }
                cout << " now removing (" << reserve[index].first << ", " << reserve[index].second << ")" << endl;
                
                reserve.erase(reserve.begin() + index);
                
                cout << "reserve after remove now contains:" << endl;
                for (pair<int, int> p : reserve) {
                    cout << "(" << p.first << ", " << p.second << "), ";
                }
                cout << endl;
            }

            reserve.push_back(dolls[i]);
        }
    }

    return m;
}  

int main() {

    int t;
    cin >> t;

    for (int i = 0; i < t; i++) {
        int m;
        cin >> m; //number of dolls in this test case.

        vector<pair<int, int>> dolls(m);
        for (int j = 0; j < m; j++) {
            int w, h;
            cin >> w >> h;
            dolls[j] = pair<int, int>(w, h);
        }

        cout << mindolls(dolls) << endl;
    }

    return 0;
}
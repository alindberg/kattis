package kattis;

import io.Kattio;

public class HumanCannonball {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		int N = io.getInt();
		for(int i = 0; i < N; i++) {
			double v0 = io.getDouble(); //Balls initial velocity in m/s
			double ang = io.getDouble(); //Cannon angle
			double x0 = io.getDouble(); //Distance from cannon to wall
			double h1 = io.getDouble(); // lower edge
			double h2 = io.getDouble(); // upper edge
			
			double t = x0 / (v0 * cos(ang));
			double y = v0 * t * sin(ang) - (9.81 * t * t) / 2.0;
			
			io.println(y - 1 >= h1 && y + 1 <= h2 ? "Safe" : "Not Safe");
		}
		
		io.close();
	}
	
	static double cos(double ang) {
		return Math.cos(ang * (Math.PI / 180));
	}

	static double sin(double ang) {
		return Math.sin(ang * (Math.PI / 180));
	}

}

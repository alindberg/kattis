package kattis;

import io.Kattio;

public class PizzaCrust {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		
		int r = io.getInt();
		int c = io.getInt(); // crust radius.. . .
		
		double area = Math.PI * Math.pow(r, 2);
		double cheese = Math.PI * Math.pow(r - c, 2);
		double perc = (cheese / area) * 100;
		
		io.println(perc);
		
		io.close();
	}

}

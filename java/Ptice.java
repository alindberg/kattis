package kattis;

import io.Kattio;

public class Ptice {
	
	private final String answer, name;
	private int c;
	
	private Ptice(String name, String answer) {
		this.name = name;
		this.answer = answer;
		
		c = 0;
	}
	
	String getAnswer() {
		return answer;
	}
	
	void addCorr() {
		c++;
	}
	
	int corr() {
		return c;
	}
	
	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		
		int n = io.getInt();
		Ptice adrian = new Ptice("Adrian", create("ABC", n));
		Ptice bruno = new Ptice("Bruno",  create("BABC", n));
		Ptice goran = new Ptice("Goran",  create("CCAABB", n));
		Ptice[] partp = {adrian, bruno, goran};
		
		String correct = io.getWord();
		for(int i = 0; i < n; i++) {
			char c = correct.charAt(i);
			for(int j = 0; j < partp.length; j++) {
				if(partp[j].getAnswer().charAt(i) == c) {
					partp[j].addCorr();
				}
			}
		}
		
		int max = 0;
		for(int i = 0; i < partp.length; i++) {
			if(partp[i].corr() > max) 
				max = partp[i].corr();
		}
		
		io.println(max);
		for(int i = 0; i < partp.length; i++) {
			if(partp[i].corr() == max) {
				io.println(partp[i].name);
			}
		}
		
		io.close();
	}
	
	private static String create(String s, int length) {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < length; i++) {
			sb.append(s.charAt(i % s.length()));
		}
		return sb.toString();
	}

}

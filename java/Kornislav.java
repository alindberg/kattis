package kattis;

import java.util.Arrays;

import io.Kattio;

public class Kornislav {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		
		int A = io.getInt();
		int B = io.getInt();
		int C = io.getInt();
		int D = io.getInt();
		
		int[] arr = {A, B, C, D};
		Arrays.sort(arr);
		io.println(arr[0] * arr[2]);
		io.close();
	}
	
	

}

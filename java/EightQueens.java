package kattis;

import java.util.ArrayList;
import java.util.List;

import Point2D;
import io.Kattio;

public class EightQueens {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);

		boolean[][] board = new boolean[8][8];
		List<Point2D> poss = new ArrayList<>();
		
		for (int y = 0; y < 8; y++) {
			String word = io.getWord();
			for (int x = 0; x < 8; x++) {
				if (word.charAt(x) == '*') {
					board[y][x] = true;
					poss.add(new Point2D(x, y));
				}
			}
		}
		if(poss.size() != 8) {
			io.println("invalid");
		} else {
						
			boolean valid = true;
			for(Point2D p : poss) {
				if(horizontal(board, p) || vertical(board, p) || leftDiagonal(board, p) || rightDiagonal(board, p)) {
					valid = false;
					break;
				}
			}
			io.println(valid ? "valid" : "invalid");
			
		}
		
		io.close();
	}
	

	private static boolean horizontal(boolean[][] board, Point2D p) {
		for(int x = 0; x < board.length; x++) {
			if(x != p.getX() && board[p.getY()][x]) return true;
		}
		return false;
	}

	private static boolean vertical(boolean[][] board, Point2D p) {
		for(int y = 0; y < board.length; y++) {
			if(y != p.getY() && board[y][p.getX()]) return true;
		}
		return false;
	}

	private static boolean leftDiagonal(boolean[][] board, Point2D p) {
		
		int lx = p.getX() - 1;
		int ly = p.getY() - 1;
		while(lx >= 0 && ly >= 0) {
			if(board[ly--][lx--]) return true;
		}
		
		int rx = p.getX() + 1;
		int ry = p.getY() + 1;
		while(rx < board.length && ry < board.length) {
			if(board[ry++][rx++]) return true;
		}
		
		return false;
	}

	private static boolean rightDiagonal(boolean[][] board, Point2D p) {
		int lx = p.getX() - 1;
		int ly = p.getY() + 1;
		while(lx >= 0 && ly < board.length) {
			if(board[ly++][lx--]) return true;
		}
		
		int rx = p.getX() + 1;
		int ry = p.getY() - 1;
		while(rx < board.length && ry >= 0) {
			if(board[ry--][rx++]) return true;
		}
		return false;
	}
}

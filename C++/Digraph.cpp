#include <iostream>
#include <vector>
#include <string>
#include "Digraph.h"

using namespace std;

Digraph::Digraph(int v): v(v) {
    adj = vector<vector<int>>(v);
}

/**
    Add directed edge from u to v.
*/
void Digraph::addEdge(int u, int v) {
    adj[u].push_back(v);
    e++;
}

vector<int> Digraph::adjacent(int v) {
    return adj[v];
}

int Digraph::vertices() {
    return v;
}

int Digraph::edges() {
    return e;
}


string Digraph::show() {
    string s;
    s += "Digraph with " + 
        std::to_string(v) + " vertices, " +
        std::to_string(e) + " edges\n";

    for (int i = 0; i < v; i++) {
        s += to_string(i) + ": ";
        for(int k : adj[i]) {
            s += to_string(k) + " ";
        }
        
        if (i < v - 1)
            s += "\n";
    }

    return s;
}



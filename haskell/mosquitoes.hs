import Data.Maybe
import Control.Monad (replicateM)
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as BC

type Diameter = Double
type Point = (Double, Double)

main = do
    n <- B.getLine
    msqs <- replicateM (readInt n) run_scenario
    mapM_ putStrLn (map show msqs)

run_scenario :: IO Int
run_scenario = do

    --get the number of mosquitoes and the diameter
    line <- B.getLine >> B.getLine 
    let [m, d] = BC.split ' ' line

    --get the coordinates of the mosquitoes, and find all pairs of coords.
    ps <- getLines (readInt m)
    let points = map readpoint ps
    let pairs = point_pairs points

    --For each pair of point, find the 0, 1 or 2 circles intersecting both,
    --and count how many mosquitoes are trapped inside.
    let caught = map (count_trapped points (readDouble d)) pairs
    return $ maximum caught

count_trapped :: [Point] -> Diameter -> (Point, Point) -> Int
count_trapped ps diameter (p1@(x1, y1), p2@(x2, y2)) = 
    let dist = distance p1 p2
        radius = diameter / 2
        leg = dist / 2
        h = sqrt $ radius^2 - leg^2

        hxcomp = h * (x1 - x2) / dist
        hycomp = h * (y1 - y2) / dist

        x3 = x2 + (x1 - x2) / 2
        y3 = y2 + (y1 - y2) / 2

        p3 = (x3 + hycomp, y3 - hxcomp)
        p4 = (x3 - hycomp, y3 + hxcomp)

        count1 = length $ filter (\x -> distance p3 x <= radius) ps
        count2 = length $ filter (\x -> distance p4 x <= radius) ps
    in if dist > diameter 
       then 1
       else max count1 count2

distance :: Point -> Point -> Double
distance (x1, y1) (x2, y2) = sqrt $ (x1 - x2)^2 + (y1 - y2)^2


--pairs xs = zip xs (tail xs)
point_pairs :: [Point] -> [(Point, Point)]
point_pairs [] = []
point_pairs (p:ps) = [(p, p') | p' <- ps] ++ point_pairs ps


readInt :: B.ByteString -> Int
readInt = fst . fromJust . BC.readInt

readDouble :: B.ByteString -> Double
readDouble = read . BC.unpack

readpoint :: B.ByteString -> Point
readpoint bs = (readDouble x, readDouble y)
    where [x, y] = BC.split ' ' bs
    
getLines :: Int -> IO [B.ByteString]
getLines n = replicateM n B.getLine

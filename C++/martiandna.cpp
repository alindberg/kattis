#include <iostream>

using namespace std;

int smallest(int dna[], int N, int reqs[], int collected[], int total_req) {

    int smallest = N + 1;
    int nuc;
    int head = 0; 
    int tail = 0;
    int cur_req = 0;

    while (head < N || tail < N) {

        //Need more nucleobases
        if (cur_req < total_req) {

            nuc = dna[tail++];

            //this nucleobase might be needed
            if (reqs[nuc] > 0) { 
                
                //collect it
                collected[nuc]++;

                //you actually needed this
                if (collected[nuc] <= reqs[nuc]) {
                    cur_req++;
                }
            }
            
        //Move the head until you dont have all the required anymore. 
        } else { 

            if (cur_req == total_req && tail - head < smallest) {
                smallest = tail - head;
                //possible early exit case
            }

            nuc = dna[head++];

            if (reqs[nuc] > 0) {
                
                collected[nuc]--;

                //If tail is exhausted, and removing one causes cur_req to be less than total, you're done
                if (collected[nuc] < reqs[nuc]) {
                    cur_req--;
                }
            }
        }

        //Can never have a substring again
        if (tail == N && cur_req < total_req) break;

    }

    return smallest;
}


int main() {
    int N; //length of DNA
    int K; //length of alphabet
    int R; //how many requirements
    cin >> N >> K >> R;

    int dna[N];

    for (int i = 0; i < N; i++) {
        cin >> dna[i];
    }

    int reqs[K] = {};
    int collected[K] = {};
    int total_req = 0;

    for (int i = 0; i < R; i++) {
        int B, Q;
        cin >> B >> Q;
        reqs[B] = Q;
        total_req += Q;
    }

    int k = smallest(dna, N, reqs, collected, total_req);
    if (k == N + 1) {
        cout << "impossible" << endl;
    } else {
        cout << k << endl;
    }

    return 0;
}
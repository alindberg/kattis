package kattis;

import io.Kattio;

public class Spavanac {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		int h = io.getInt();
		int m = io.getInt();
	

		
		if(m < 45) {
			h = Math.floorMod(h - 1, 24);
			m = Math.floorMod(m - 45, 60);
			
		} else {
			m -= 45;
		}

		System.out.println(h + " " + m);
		
		io.close();
	}

}

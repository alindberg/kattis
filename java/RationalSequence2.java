package kattis;

import io.Kattio;

public class RationalSequence2 {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		
		int n = io.getInt();
		for(int i = 0; i < n; i++) {
			int k = io.getInt();
			String[] w = io.getWord().split("/");
			int p = Integer.parseInt(w[0]);
			int q = Integer.parseInt(w[1]);
			String path = path(p, q);

			long j = 1;
			for(char c : path.toCharArray()) {
				j *= 2;
				if(c == 'R') {
					j++;
				}
			}

			io.println(k + " " + j);
		}
		
		io.close();
	}
	
	
	static String path(int p, int q) {
		StringBuilder path = new StringBuilder();		
		while(p + q > 2) {
			if(p < q) { 
				q = q - p;
				path.append("L");
			} else if(p > q) {
				p = p - q;
				path.append("R");
			} 
		}
		
		return path.reverse().toString();
	}
	

}

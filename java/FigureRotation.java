package kattis;

import java.util.Scanner;

public class FigureRotation {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		String[][] figures = new String[100][100];
		int y = -1;
		
		int n;
		while((n = Integer.parseInt(sc.nextLine())) != 0) {
			figures[++y] = new String[n];
			int maxW = 0;
			for(int x = 0; x < n; x++) {
				figures[y][x] = sc.nextLine();
				if(figures[y][x].length() > maxW) {
					maxW = figures[y][x].length();
				}
			}
			figures[y] = rotate(figures[y], maxW);
		}
		
		show(figures, y);
		sc.close();
	}
	
	static String[] rotate(String[] xs, int w) {
		char[][] css = new char[w][xs.length];
		
		for(int i = 0; i < xs.length; i++) {
			for(int j = 0; j < w; j++) {
				char z;
				if(j < xs[i].length()) {
					z = flipChar(xs[i].charAt(j));
				} else {
					z = ' ';
				}
				css[j][xs.length - i - 1] = z;
			}
		}
		
		xs = new String[w];
		for(int i = 0; i < css.length; i++) {
			int j = css[i].length - 1;
			for(; j >= 0; j--) {
				if(!Character.isWhitespace(css[i][j])) {
					break;
				}
			}
			xs[i] = new String(css[i]).substring(0, j + 1);
		}
		
		return xs;
	}
	
	static char flipChar(char z) {
		if(z == '-') {
			return '|';
		} else if(z == '|') {
			return '-';
		} else {
			return z;
		}
	}
	
	static void show(String[][] xss, int y) {
		for(int i = 0; i <= y; i++) {
			for(int j = 0; j < xss[i].length; j++) {
				System.out.println(xss[i][j]);
			}
			if(i < y) {
				System.out.println();
			}
		}
	}


}

#include <iostream>
#include <vector>
#include <climits>
#include <queue>

using namespace std;

typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<pii> vpii;
typedef vector<vpii> vvpii;

#define vertex second
#define weight first

vi dijkstra(vvpii &adj, int s) {

    vi dist(adj.size(), INT_MAX);
    dist[s] = 0;

    priority_queue<pii, vector<pii>, greater<pii>> pq;
    pq.push(pii(0, s));

    //add all others with infinity priority. then decreaseKey

    while (!pq.empty()) {
        int u = pq.top().vertex;
        pq.pop();   
        
        for (auto &p : adj[u]) {

            int w = p.weight;
            int v = p.vertex;

            if (dist[u] + w < dist[v]) {
                dist[v] = dist[u] + w;
                pq.push(pii(dist[v], v));
            }
        }
    }

    return dist;
}


int main() {
    ios::sync_with_stdio(false); 
    cin.tie(NULL); 

    int n, m, q, s;
    cin >> n >> m >> q >> s;

    while (n + m + q + s > 0) {
        vvpii adj(n);

        for (int i = 0; i < m; i++) {
            int u, v, w;
            cin >> u >> v >> w;
            adj[u].push_back(pii(w, v));
        }
        
        vi dist = dijkstra(adj, s);

        for (int i = 0; i < q; i++) {
            int t;
            cin >> t;
            if (dist[t] == INT_MAX) {
                cout << "Impossible\n";
            } else {
                cout << dist[t] << "\n";
            }
        }

        cout << endl;
        cin >> n >> m >> q >> s;
    }

    
    return 0;
}

package kattis;

import Kattio;

public class Beavergnaw {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		
		int D = io.getInt();
		int V = io.getInt();
		while(D + V != 0) {
			io.println(Math.cbrt( (((-6) * V) / Math.PI) + (D*D*D)));
			
			D = io.getInt();
			V = io.getInt();
		}
		
		io.close();
	}

}

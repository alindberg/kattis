using System;

public class Teque {

    private Node first, middle, last;
    private int count = 0;

    private class Node {
        public Node left, right;
        public int data;
        public Node (int x) { data = x; }
    }

    private void InitQueue(int x) {
        first = new Node(x);
        last = first;
        middle = first;
        count = 1;
    }

    public bool Empty() {
        return count == 0;
    }

    public void PushFront(int x) {

        if (Empty()) { 
            InitQueue(x); 
            return; 
        }

        first.left = new Node(x);
        first.left.right = first;
        first = first.left;
        count++;

        if (count % 2 != 0) {
            middle = middle.left;
        }
    }

    public void PushBack(int x) {

        if (Empty()) { 
            InitQueue(x); 
            return; 
        }

        last.right = new Node(x);
        last.right.left = last;
        last = last.right;
        count++;
        
        if (count % 2 == 0) {
            middle = middle.right;
        }
    }

    public void PushMid(int x) {

        if (Empty()) {
            InitQueue(x);
            return;
        }

        var node = new Node(x);
        count++;

        if (count % 2 == 0) {

            node.left = middle;

            if (count > 2) {
                middle.right.left = node;
                node.right = middle.right;
            }

            middle.right = node; 

            if (count == 2) {
                last = node;
            }

        } else {
            node.right = middle;
            node.left = middle.left;
            middle.left.right = node;
            middle.left = node;
        }

        middle = node;
    }

    //Print ith index element (0-based)
    public void Print(int i) {

        if (i == count - 1) {
            Console.WriteLine(last.data);
            return;
        }

        //middle is at 
        int mid = 0;
        if (count % 2 != 0) {
            mid = count / 2;
        } else {
            mid = (count + 1 ) / 2;
        }

        if (i == mid) {
            Console.WriteLine(middle.data);
            return;
        }
        

        Node current = (i < mid) ? first : middle.right;
        int index = (i < mid) ? 0 : mid+1;

        // Node current = first;
        // int index = 0;
        while (current.right != null) {

            if (index == i) {
                Console.WriteLine(current.data);
                return;
            }
            current = current.right;
            index++;
        }

    }

    static void run() {
        int N = Convert.ToInt32(Console.ReadLine());

        int x;
        string line;
        Teque teq = new Teque();
        for (int i = 0; i < N; i++) {

            line = Console.ReadLine();
            var words = line.Split(' ');
            x = Convert.ToInt32(words[1]);
            
            switch (words[0]) {
                case "push_front":
                    teq.PushFront(x);
                    break;
                case "push_back":
                    teq.PushBack(x);
                    break;
                case "push_middle":
                    teq.PushMid(x);
                    break;
                case "get":
                    teq.Print(x);
                    break;
                default:
                    Console.WriteLine("wtf?");
                    throw new Exception("jaja");
            }
        }
    }

    static void Main(string[] args) {
        run();
    }
}
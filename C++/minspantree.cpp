#include <iostream>
#include <vector>
#include <climits>
#include <algorithm>
#include <queue>

using namespace std;

#define inf INT_MAX
#define weight first
#define vertex second

typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;

typedef pair<int, int> pii;
typedef vector<pii> vpii;
typedef vector<vpii> vvpii;

//Prim's algorithm
void mst(vvpii &adj) {

    int n = adj.size();
    vi prev(n, inf);
    vi cost(n, inf);
    vb visited(n, false);

    //Pick any initial node.
    cost[0] = 0;

    priority_queue<pii, vector<pii>, greater<pii>> pq;
    pq.push(pii(0, 0));

    while (!pq.empty()) {
        int u = pq.top().vertex;
        pq.pop();
        visited[u] = true;
        
        for (auto &p : adj[u]) {
            int w = p.weight;
            int v = p.vertex;

            //if there is an edge from u, pointing to an unvisited node v, with
            if (w < inf && !visited[v] && cost[v] > w) {
                cost[v] = w;
                prev[v] = u;
                pq.push(pii(w, v));
            }
        }
    }

    long long total_cost = 0;
    vector<pii> tree;

    for (int i = 1; i < n; i++) {
        if (cost[i] == inf) {
            cout << "Impossible\n";
            return;
        }

        total_cost += cost[i];
        
        if (i > prev[i]) {
            tree.push_back(pii(prev[i], i));
        } else {
            tree.push_back(pii(i, prev[i]));
        }

    }

    sort(tree.begin(), tree.end());

    cout << total_cost << endl;
    for (int i = 0; i < tree.size(); i++) {

        cout << tree[i].first << " " << tree[i].second << endl;
    }

}


int main() {
    ios::sync_with_stdio(false); 
    cin.tie(NULL); 

    int n, m;
    cin >> n >> m;

    while (n) {

        vvpii adj(n);

        for (int i = 0; i < m; i++) {
            int u, v, w;
            cin >> u >> v >> w;

            adj[u].push_back(pii(w, v));
            adj[v].push_back(pii(w, u));
        }
        
        mst(adj);

        cin >> n >> m;
    }

    return 0;
}

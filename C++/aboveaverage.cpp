#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include <algorithm>
#include <iomanip>

using namespace std;

typedef vector<int> vi;
typedef vector<bool> vb;

typedef pair<int, int> pii;
typedef vector<pii> vii;

#define x first
#define y second

int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)
    cout.tie(NULL);

    int C, N, grade, sum, count;
    float avg;

    cin >> C; 
    for (int i = 0; i < C; i++) {
        cin >> N;
        vi grades(N);

        sum = 0;
        for (int j = 0; j < N; j++) {
            cin >> grade;
            grades.push_back(grade);
            sum += grade;
        }

        avg = sum / N;
        count = 0;
        for (int k : grades) {
            if (k > avg) {
                count ++;
            }
        }

        printf("%.3f%%\n", 100*(float)count/N);
        //cout << (100 * ((float)count / N)) << '%' << endl;
    }

    return 0;
}
package kattis;

import io.Kattio;

public class Sibice {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		int n = io.getInt();
		int w = io.getInt();
		int h = io.getInt();

		for(int i = 0; i < n; i++) {
			int c = io.getInt();
			if(c <= Math.sqrt(w*w + h*h)) {
				System.out.println("DA");
			} else {
				System.out.println("NE");
			}
		}
		io.close();
		
	}

}

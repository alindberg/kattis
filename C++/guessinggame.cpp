#include <iostream>
#include <vector>
#include <string>

using namespace std;

int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)

    int x;
    cin >> x;

    int lo = 0;
    int hi = 11;

    string reply;
    while (x) {
        cin.ignore();
        getline(cin, reply);

        if (reply == "too high" && x < hi) {
            hi = x;
        } else if (reply == "too low" && x > lo) {
            lo = x;
        } else if (reply == "right on") {
            if (x > lo && x < hi) {
                cout << "Stan may be honest" << endl;
            } else {
                cout << "Stan is dishonest" << endl;
            }
            lo = 0;
            hi = 11;
        }

        cin >> x;
    }
    
    return 0;
}

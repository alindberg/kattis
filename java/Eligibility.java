package kattis;

import io.Kattio;

public class Eligibility {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		
		int t = io.getInt(); // cases
		for(int i = 0; i < t; i++) {
			String name = io.getWord(); // Name of student
			int ys = Integer.parseInt(io.getWord().split("/")[0]);
			int yb = Integer.parseInt(io.getWord().split("/")[0]);
			int courses = io.getInt();
			
			if(ys >= 2010 || yb >= 1991) {
				io.println(name + " eligible");
			} else if(courses >= 41){
				io.println(name + " ineligible");
			} else {
				io.println(name + " coach petitions");
			}
			
		}
		
		io.close();

	}

}

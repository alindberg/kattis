main = interact (show . solve . words)

solve :: [String] -> Int
solve ts = case map read ts :: [Int] of
    (t:ts') -> length $ filter (\x -> x < 0) ts'
    _      -> error "wtf"
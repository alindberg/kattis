#ifndef Graph_H
#define Graph_H

#include <vector>
#include <string>

using std::vector;
using std::string;

class Graph {

    const int v;
    int e;
    vector<vector<int>> adj;

public:
    Graph(int v);
    void addEdge(int u, int v);
    vector<int> adjacent(int v);
    int vertices();
    int edges();
    string show();
};


#endif
#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include <algorithm>
#include <set>

using namespace std;

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<bool> vb;

typedef pair<int, int> pii;
typedef vector<pii> vii;

#define x first
#define y second

int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)
    cout.tie(NULL);

    int n;
    cin >> n;
    while (n > 0) {

        vvi graph(n, vi(n));
        
        for (int y = 0; y < n; y++) {
            for (int x = 0; x < n; x++) {
                cin >> graph[y][x];
            }
        }

        set<int> weak;
        for (int i = 0; i < n; i++) {
            bool is_weak = true;
            for (int j = 0; j < n && is_weak; j++) {
                if (i != j && graph[i][j]) {
                    for (int k = 0; k < n && is_weak; k++) {
                        if (j != k && graph[j][k] && graph[i][k]) {
                            is_weak = false;
                        }
                    }
                }
            }

            if (is_weak) weak.insert(i);
        }

        vector<int> vweak(weak.begin(), weak.end());
        sort(vweak.begin(), vweak.end());
        for (const int &i : vweak) {
            cout << i << " ";
        }
        cout << endl;

        cin >> n;
    }

    return 0;
}
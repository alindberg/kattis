package kattis;

import io.Kattio;

public class SpeedLimit {
	
	
	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		
		int n = io.getInt();
		while(n != -1) {
			int[][] pairs = new int[n][2];
			for(int i = 0; i < n; i++) {
				pairs[i][0] = io.getInt(); //speed
				pairs[i][1] = io.getInt(); //hours.
			}
			
			int sum = pairs[0][0] * pairs[0][1];
			for(int j = 1; j < n; j++) {
				sum += pairs[j][0] * (pairs[j][1] - pairs[j - 1][1]);
			}
			
			io.println(sum + " miles");
			
			
			n = io.getInt();
		}
		
		io.close();
	}
	

}

#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <algorithm>
#include "Digraph.h"

using namespace std;

typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<bool> vb;
typedef vector<pii> vpii;

//time limit exceeded. Trenger O(nlogn) løsning

class DFS {
    Digraph graph;
    vb visited;
    int time = 0;

    public:
    vi post;

    DFS(Digraph &graph): graph(graph) {

        visited = vb(graph.vertices(), false);
        post = vi(graph.vertices());

        for (int u = 0; u < graph.vertices(); u++) {
            if (!visited[u]) {
                explore(u);
            }
        }
    }

    void explore(int u) {
        visited[u] = true;
        time++;

        for (int v : graph.adjacent(u)) {
            if (!visited[v]) {
                explore(v);
            }
        }

        post[u] = time;
        time++;
    }
};

void construct_graphs(Digraph &graph, Digraph &reversed, vi &weights) {

    for (int i = 0; i < weights.size(); i++) {
        for (int j = i + 1; j < weights.size(); j++) {
            if (weights[j] > weights[i]) {
                graph.addEdge(i, j);
                reversed.addEdge(j, i);
            }
        }
    }
}


int main() {
    ios::sync_with_stdio(false); 
    cin.tie(NULL); 

    int n;

    while (cin >> n) {
        
        vi weights(n);

        for (int i = 0; i < n; i++) {
            cin >> weights[i];
        }
        
        Digraph graph(n);
        Digraph reversed(n);
        construct_graphs(graph, reversed, weights);

        DFS dfs(graph);
        vpii pairs(n);
        
        for (int i = 0; i < n; i++) {
            pairs[i] = pii(dfs.post[i], i);
        }

        sort(pairs.rbegin(), pairs.rend());

        vi dp(n, 1);
        vi prev(n, -1); //-1 if source
        
        int argmax;
        int maxlen = 0;

        for (auto p : pairs) {
            int u = p.second;

            int m = 0;
            int maxpred = -1;
            for (int v : reversed.adjacent(u)) {
                if (dp[v] > m) {
                    m = dp[v];
                    maxpred = v;
                }
            }

            prev[u] = maxpred;

            dp[u] += m;

            if (dp[u] > maxlen) {
                argmax = u;
                maxlen = dp[u];
            }
        }


        cout << maxlen << endl;

        //follow path from argmax to source
        int sink = argmax;
        vi path;
        path.push_back(sink); //start with the sink
        while (prev[sink] != -1) {
            path.push_back(prev[sink]);
            sink = prev[sink];
        }

        //path.push_back(sink);
        for (int i = path.size() - 1; i >= 0; i--) {
            cout << path[i] << " ";
        }
        cout << endl;
    }

    return 0;
}
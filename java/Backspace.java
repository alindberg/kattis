package kattis;

import java.util.Stack;

import io.Kattio;

public class Backspace {
	
	static String bs(String xs) {
		Stack<Character> stack = new Stack<>();
		for(int i = 0; i < xs.length(); i++) {
			if(xs.charAt(i) == '<') {
				stack.pop();
			} else {
				stack.push(xs.charAt(i));
			}
			
		}
		
		StringBuilder sb = new StringBuilder();
		while(!stack.isEmpty()) {
			sb.append(stack.pop());
		}
		
		return sb.reverse().toString();

	}
	
	
	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		io.println(bs(io.getWord()));
		io.close();
	}

}

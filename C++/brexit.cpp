#include <iostream>
#include <vector>
#include <string>
#include <queue>
#include <cmath>
#include <algorithm>

using namespace std;

typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<bool> vb;

typedef pair<int, int> pii;
typedef vector<pii> vii;

int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)
    cout.tie(NULL);

    //countries, partnerships, home country, first leaver.
    int c, p, x, l, u, v;

    cin >> c >> p >> x >> l;

    vvi graph(c);
    
    for (int i = 0; i < p; i++) {
        cin >> u >> v;
        graph[u-1].push_back(v-1);
        graph[v-1].push_back(u-1);
    }

    if (x == l) {
        cout << "leave" << endl;
        return 0;
    }

    vi threshold(c); //how many leaving neighbors it can handle. if 0, it has left.
    for (int u = 0; u < c; u++) {
        threshold[u] = (graph[u].size() + 1) / 2; 
    }
    threshold[l-1] = 0;

    queue<int> Q;
    Q.push(l-1);

    while (!Q.empty()) {
        u = Q.front();
        Q.pop();

        for (int v : graph[u]) {
            
            if (threshold[v] > 0) {
                --threshold[v];
                if (threshold[v] == 0) {
                    Q.push(v);
                }
            }
        }

    }

    if (threshold[x-1] == 0) {
        cout << "leave" << endl;
    } else {
        cout << "stay" << endl;
    }

    return 0;
}
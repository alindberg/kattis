#include <iostream>
#include <cstdlib>
#include "SegtreeIQ.h"

SegtreeIQ::SegtreeIQ(int n) {
    N = 2;
    while (2 * n + 3 > N) N *= 2;

    offs = 2;
    while (n + 2 > offs) offs *= 2;

    A = new long[N](); 
}


long SegtreeIQ::getValue(int i) {

    int pos = offs + i;

    long ans = 0;
    do {
        ans += A[pos];
        pos /= 2;
    } while (pos > 0);

    return ans;
}


void SegtreeIQ::update(int i, int j, int diff) {
    int L = i + offs;
    int R = j + offs; 

    if (L == R) {
        A[L] += diff;
        return;
    }

    bool LL = false;
    bool RR = false; 
    

    while (true) {

        bool Lright = L % 2 == 0;
        bool Rleft = R % 2 == 1;

        L /= 2;
        R /= 2;

        if (L == R) {

            if (!RR && !LL) {
                A[L] += diff;
                
            } else if (!LL && RR) {
                A[2 * L] += diff;

            } else if (LL && !RR) {
                A[2 * R + 1] += diff;
            } 

            break;
        }


        //Left side walked up right
        if (Lright) {
            
            //Walked left previously, update right child only.
            if (LL) {
                A[2 * L + 1] += diff;
            } 

        //Walked left for the first time.
        } else if (!LL) {
            LL = true;
            A[2 * L + 1] += diff;
        }

        //Right side walked up left
        if (Rleft) {
            //Never walked right before
            if (RR) {
               A[2 * R] += diff;
            }

        //Walked right for the first time.
        } else if (!RR) {
            RR = true;
            A[2 * R] += diff;
        }
    
    }
}

#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Planet;
int islands(Planet img);
void explore(int x, int y, Planet& img);

struct Pos {
    int x, y;
    Pos(int x, int y): x(x), y(y) {}
};

class Planet {
    vector<string> img;
    vector<vector<bool>> visited;

    public:
    int rows, cols;

    Planet(vector<string> img): img(img) {
        rows = img.size();
        cols = img[0].size();
        visited = vector<vector<bool>>(rows, vector<bool>(cols));
    }

    void visit(int x, int y) {
        visited[y][x] = true;
    }

    bool isVisited(int x, int y) {
        return visited[y][x];
    }

    bool isLand(int x, int y) {
        return img[y][x] == 'L';
    }

    bool isWater(int x, int y) {
        return img[y][x] == 'W';
    }

    vector<Pos> neighborsOf(int x, int y) {
        Pos candidates[] = {Pos(x, y - 1), Pos(x - 1, y), Pos(x + 1, y), Pos(x, y + 1)};

        vector<Pos> neighborsOf;
        for (Pos pos: candidates) {
            if(pos.y < 0 || pos.y >= rows || pos.x < 0 || pos.x >= cols) continue;

            neighborsOf.push_back(pos);
        }

        return neighborsOf;
    }

}; 

/*
    Returns the minimum number of islands on the planet.
*/
int islands(Planet img) {

    int minIslands = 0;

    for(int y = 0; y < img.rows; y++) {
        for (int x = 0; x < img.cols; x++) {
            
            if(img.isWater(x, y)) {
                img.visit(x, y);
            }
            
            if (img.isLand(x, y) && !img.isVisited(x, y)) {

                //printf("Found land. Starting explore from (%d, %d) (and increasing minIslands).\n", x, y);
                explore(x, y, img);
                minIslands += 1;
            }
        }
    }

    return minIslands;
}

void explore(int x, int y, Planet& img) {
    img.visit(x, y);

    for (Pos pos : img.neighborsOf(x, y)) {

        if (!img.isWater(pos.x, pos.y) && !img.isVisited(pos.x, pos.y)) {
            //printf("not water, exploring(%d, %d)\n", pos.x, pos.y);
            explore(pos.x, pos.y, img);
        }
    }

}

int main() {

    int r, c;
    cin >> r >> c;

    vector<string> lines;
    cin.ignore(); //ignore \n from getline
    for (int y = 0; y < r; y++) {
        string line;
        getline(cin, line);
        lines.push_back(line);
    }

    Planet g(lines);

    cout << islands(g) << endl;
    return 0;
}
#include <iostream>
#include <string>
#include <vector>
#include <cstring>
#include <unordered_map>

using namespace std;

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<string> vs;
typedef vector<vb> vvb;

#define x first
#define y second

struct Node {
    Node* children[26];

    Node() {
        memset(children, 0, sizeof(children));
    }
};

int width, height;
vs grid;

vector<pii> adjacent(int x, int y) {
    pii candidates[] = {pii(x, y - 1), pii(x - 1, y), pii(x + 1, y), pii(x, y + 1)};

    vector<pii> adj;
    for (pii &pos : candidates) {
        if (pos.y >= 0 && pos.y < height && pos.x >= 0 && pos.x < width) {
            adj.push_back(pos);
        }
    }

    return adj;
}

void insert(Node *node, const char c) {
    if (!node->children[c - 'A']) {
        node->children[c - 'A'] = new Node();
    }
}


void explore(int x, int y, Node *root, vvb &visited, int d) {

    if (d > 10) return;
    
    visited[y][x] = true;
    insert(root, grid[y][x]);

    for (pii &p : adjacent(x, y)) {
        if (!visited[p.y][p.x]) {
            explore(p.x, p.y, root->children[grid[y][x] - 'A'], visited, d + 1);
        }
    }
    
    visited[y][x] = false;
}


bool contains(Node* root, string &str) {

    for (char c : str) {
        if (!root->children[c - 'A']) {
            return false;
        }
        root = root->children[c - 'A'];
    }

    return true;

}


int main() {
    cin >> height >> width;

    grid = vs(height);
    for (int i = 0; i < height; i++) {
        cin >> grid[i];
    }

    Node *root = new Node();

    vvb visited(height, vb(width, false));
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            explore(x, y, root, visited, 0);
        }
    }

    int n;
    cin >> n;

    string word;
    int result = 0;

    for (int i = 0; i < n; i++) {

        cin >> word;
        if (contains(root, word)) {
            result++;
        }
    }

    cout << result << endl;

    return 0;
}




/*
void insert(Node* node, const char *s) {
    if (*s) {
        if (!node->children[*s - 'a'])
            node->children[*s - 'a'] = new Node();

        insert(node->children[*s - 'a'], s + 1);

    } else {
        node->is_end = true;
    }
}


*/
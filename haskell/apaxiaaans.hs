main = interact removeSeq


removeSeq :: String -> String
removeSeq [] = []
removeSeq [x] = [x]
removeSeq (x:y:xs) | x == y = removeSeq (y:xs)
                   | otherwise = x : removeSeq (y:xs)

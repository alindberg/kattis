#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

typedef pair<double, double> point;

#define x first
#define y second

double dist(point p1, point p2) {
    return sqrt(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2));
}


//How many points are inside the circle with center p and radius r.
int count_trapped(point p, double r, vector<point> &pts) {

    int ins = 0;
    for (point pt : pts) {
        if (dist(p, pt) <= r) {
            ins++;
        }
    }
    
    return ins;
}


int trap_mosquitoes(vector<point> &pts, double diameter) {

    double distance;
    double r = diameter / 2; 
    int best = 0;

    for (int i = 0; i < pts.size(); i++) {
        for(int j = i + 1; j < pts.size(); j++) {

            distance = dist(pts[i], pts[j]);

            if (distance > diameter && best == 0) {
                best = 1;

            } else {

                double a = distance / 2;
                double h = sqrt(r * r - a * a);

                double hycomp = h * (pts[j].y - pts[i].y) / distance; 
                double hxcomp = h * (pts[j].x - pts[i].x) / distance; 

                double x2 = pts[i].x + (pts[j].x - pts[i].x) / 2;
                double y2 = pts[i].y + (pts[j].y - pts[i].y) / 2;


                double ins1 = count_trapped(point(x2 + hycomp, y2 - hxcomp), r, pts);
                double ins2 = count_trapped(point(x2 - hycomp, y2 + hxcomp), r, pts);
                double mx = max(ins1, ins2);

                if (mx > best) {
                    best = mx;
                }

            }
        }
    }

    return best;
}


int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)


    int n; 
    cin >> n;

    for (int i = 0; i < n; i++) {
        
        int m;
        double d;

        cin >> m >> d;

        vector<point> pts(m);

        for (int j = 0; j < m; j++) {
            double xj, yj;
            cin >> xj >> yj;
            pts[j] = point(xj, yj);
        }

        cout << trap_mosquitoes(pts, d) << endl;
    }


    return 0;
}
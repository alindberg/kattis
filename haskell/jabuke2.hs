import Numeric (showFFloat)
import Control.Monad (replicateM)

type Point = (Int, Int)
data Shape = Triangle Point Point Point

main = do
    (a:b:c:t:_) <- getLines 4
    ts <- getLines (read t)
    let triangle = Triangle (readPoint a) (readPoint b) (readPoint c)
    let trees = map readPoint ts

    putStrLn $ format (area triangle) 1

    let ins = filter (inside triangle) trees
    putStrLn . show . length $ ins


inside :: Shape -> Point -> Bool
inside tri@(Triangle a b c) p = 
    let area1 = area $ Triangle p a b
        area2 = area $ Triangle p b c
        area3 = area $ Triangle p a c
    in area tri == area1 + area2 + area3

area :: Shape -> Double
area (Triangle (x1, y1) (x2, y2) (x3, y3)) = (1/2) * fromIntegral s 
    where s = abs $ x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)

readPoint :: String -> Point
readPoint nums = (read x, read y)
    where [x, y] = words nums

format :: RealFloat a => a -> Int -> String
format x decimals = showFFloat (Just decimals) x ""

getLines :: Int -> IO [String]
getLines n = replicateM n getLine
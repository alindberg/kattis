package kattis;

import java.util.HashMap;
import java.util.Map;

import Kattio;

public class ACMContest {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		
		//submission time, id letter, result
		//Primary measure: How many problems solved.
		//Secondary m: combination of time and penalties
		//=> team score = sum of submission times with right ans + 20 min penalty for each wrong
		Map<Character, Integer> map = new HashMap<>();
		int solved = 0;
		int mins = io.getInt();
		while(mins != -1) {
			char id = io.getWord().charAt(0);
			boolean success = io.getWord().equals("right");
			if(success) solved++;
			
			if(map.containsKey(id)) {
				if(success) {
					map.put(id, (-1) * map.get(id) + mins);
				} else {					
					map.put(id, map.get(id) + -20);
				}
			} else {
				map.put(id, success ? mins : -20);
			}
			mins = io.getInt();
		}
		int time = 0;
		for(Integer p : map.values()) {
			if(p >= 0)
				time += p;
		}
		io.println(solved + " " + time);
		
		io.close();
	}

}

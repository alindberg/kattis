package kattis;

import io.Kattio;

public class DetailedDifferences {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		
		int pairs = io.getInt();
		for(int i = 0; i < pairs; i++) {
			String w1 = io.getWord();
			String w2 = io.getWord();
			StringBuilder sb = new StringBuilder();
			for(int j = 0; j < w1.length(); j++) {
				if(w1.charAt(j) == w2.charAt(j)) {
					sb.append(".");
				} else {
					sb.append("*");
				}
			}
			io.println(w1);
			io.println(w2);
			io.println(sb.toString());
			if(i < pairs - 1)
				io.println();
		}
		
		
		io.close();
	}

}

main = do
    line <- getLine
    let stones = read line :: Int
    putStrLn (if mod stones 2 == 0 then "Bob" else "Alice")
    

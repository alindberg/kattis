
import java.util.ArrayList;
import java.util.List;

/**
 * Assumes y growing downwards..
 *
 */
public class Point2D {

	private int x, y;
	
	public Point2D(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public static Point2D west(Point2D p) {
		return new Point2D(p.getX() - 1, p.getY());
	}
	
	public static Point2D east(Point2D p) {
		return new Point2D(p.getX() + 1, p.getY());
	}
	
	public static Point2D north(Point2D p) {
		return new Point2D(p.getX(), p.getY() - 1);
	}

	public static Point2D south(Point2D p) {
		return new Point2D(p.getX(), p.getY() + 1);
	}
	
	public static Point2D northwest(Point2D p) {
		return new Point2D(p.getX() - 1, p.getY() - 1);
	}
	
	public static Point2D northeast(Point2D p) {
		return new Point2D(p.getX() + 1, p.getY() - 1);
	}
	
	public static Point2D southwest(Point2D p) {
		return new Point2D(p.getX() - 1, p.getY() + 1);
	}
	
	public static Point2D southeast(Point2D p) {
		return new Point2D(p.getX() + 1, p.getY() + 1);
	}
	
	/**
	 * 
	 * @param x
	 * @param y
	 * @param width
	 * @return x + y * width
	 */
	public static int toIndex(int x, int y, int width) {
		return x + y * width;
	}
	
	/**
	 * @see #toIndex(int, int, int)
	 */
	public static int toIndex(Point2D p, int width) {
		return toIndex(p.getX(), p.getY(), width);
	}
	
	/**
	 * Neighbours in east, west, north and south.
	 * @param p
	 * @param w
	 * @param h
	 * @return
	 */
	public static Iterable<Point2D> neighbours4(Point2D p, int w, int h) {
		List<Point2D> ngbhs = new ArrayList<>(4);
		if(p.getY() > 0) ngbhs.add(north(p));
		if(p.getX() < w - 1) ngbhs.add(east(p));
		if(p.getY() < h - 1) ngbhs.add(south(p));
		if(p.getX() > 0) ngbhs.add(west(p));

		return ngbhs;
	}
	
	public String toString() {
		return "(" + x + ", " + y + ")";
	}
	
}

import Data.Set as Set (toList, fromList)
import Data.Map as Map (toList, fromListWith)

main = do
    contents <- getContents
    mapM_ print $ map solve (words contents)
  
  
solve :: String -> Integer
solve xs = let n = length xs
               k = (length . dedup) xs
           in if n == k 
              then fact (fromIntegral n)
              else div ((fact . fromIntegral) n) ((product . map fact . map snd . countOcc) xs)
           


fact :: Integer -> Integer
fact x = product [1..x]


dedup :: Ord a => [a] -> [a]
dedup = Set.toList . Set.fromList

countOcc :: [Char] -> [(Char, Integer)]
countOcc xs = Map.toList $ Map.fromListWith (+) [(c, 1) | c <- xs]
#include <iostream>
#include <vector>
#include <string>
#include <cmath>

using namespace std;

int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)

    long long n;
    cin >> n;
    int k = 0;

    for (int i = 2; i <= sqrt(n); i++) {
        

        while (n % i == 0) {
            k++;
            n /= i;
        }

    }

    //a prime remains
    if (n != 1) k++;

    cout << k << endl;

}
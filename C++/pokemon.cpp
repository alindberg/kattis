#include <iostream>
#include <vector>
#include <string>
#include <climits>
#include <unordered_map>
#include <map>

using namespace std;

typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<vector<int>> vvi;

#define min(a,b) ((a)<(b)?(a):(b))
#define x first
#define y second

int inf = INT_MAX / 2;

//For each unique position, hold a set of which unique pokemons are there.
map<pii, int> pokes;

vector<pii> points;

int distance(pii &p1, pii &p2);
vvi calc_distances(vector<pii> &points);

int main() {
    ios::sync_with_stdio(false); 
    cin.tie(NULL); 

    //Number of pokestops
    int n;
    cin >> n;

    //Map each unique pokemon to a unique number.
    unordered_map<string, int> pokeid;

    int num_pokemons = 0;

    for (int i = 0; i < n; i++) {
        int r, c;
        string pokemon; 
        cin >> r >> c >> pokemon;

        pii point(c, r);

        if (!pokes.count(point)) {
            points.push_back(point);
        }
        
        if (!pokeid.count(pokemon)) {
            pokeid[pokemon] = num_pokemons++;
        }

        //add pokemon to the set at this location. pokemon i at position 2^i
        pokes[point] |= 1 << pokeid[pokemon];
    }


    vvi dist = calc_distances(points); 

    int p = 1 << num_pokemons;

    //dp[mask][i] minimum path to the pokemons in mask, ending at position i
    vvi dp(p, vi(points.size(), inf));

    pii start(0, 0);

    for (int i = 0; i < points.size(); i++) {
        dp[pokes[points[i]]][i] = distance(points[i], start);
    }

    /*
    cout << "DEBUGGGGGGGG" << endl;
        //DEBUG
    cout << " dp = " << endl;
    for (int i = 0; i < dp.size(); i++) {
        for (int j = 0; j < dp[i].size(); j++) {
        
            if (dp[i][j] == inf) {
                cout << "- ";
            } else {
                cout << dp[i][j] << " ";
            }
        }
        cout << endl;
    }

    cout << "END DEBUGGGGGGGG" << endl;
    */

    for (int mask = 1; mask < p; mask++) {
        
        for (int i = 0; i < points.size(); i++) {

            if (mask & pokes[points[i]]) {

                for (int j = 0; j < points.size(); j++) {

                    if (i != j && mask & pokes[points[j]]) {

                        dp[mask][i] = min(dp[mask ^ pokes[points[i]]][j] + dist[i][j], dp[mask][i]);
                    }
                }
            }
        }
    }

/*
    //DEBUG
    cout << " dp = " << endl;
    for (int i = 0; i < dp.size(); i++) {
        for (int j = 0; j < dp[i].size(); j++) {
        
            if (dp[i][j] == inf) {
                cout << "- ";
            } else {
                cout << dp[i][j] << " ";
            }
        }
        cout << endl;
    }
    //END DEBUG
*/
    //search in dp[p-1][i] +     distance(points[i], start) for best.

    int best = inf;
    for (int i = 0; i < points.size(); i++) {
        if (dp[p-1][i] + distance(points[i], start) < best) {
            best = dp[p-1][i] + distance(points[i], start);
        }
    }

    cout << best << endl;
    
    return 0;
}

int distance(pii &p1, pii &p2) {
    return abs(p1.x - p2.x) + abs(p1.y - p2.y);
}

vvi calc_distances(vector<pii> &points) {

    vvi d(points.size(), vi(points.size(), 0));
    for (int i = 0; i < points.size(); i++) {
        for (int j = i + 1; j < points.size(); j++) {
            int dist = distance(points[i], points[j]);
            d[i][j] = dist;
            d[j][i] = dist;
        }
    }

    return d;
}


main = interact (show . solve . readInput)

readInput x = read x :: Int

solve :: Int -> Int
solve = fromBinStr . reverse . toBinStr


toBinStr :: Int -> String
toBinStr 0 = "0"
toBinStr n = f n (floor (log (fromIntegral n) / log 2.0))
    where f n (-1) = []
          f n x | n >= 2^x = '1':f (n - 2^x) (x - 1)
                | otherwise = '0':f n (x - 1) 

fromBinStr :: String -> Int
fromBinStr [] = 0
fromBinStr ('1':xs) = 2 ^ (length xs) + fromBinStr xs
fromBinStr ('0':xs) = fromBinStr xs
import Data.Char

main = interact solve

solve :: String -> String
solve [] = []
solve (x : xs) = 
    case lookup (toLower x) lang of
        Just w -> w ++ solve xs
        Nothing -> x : solve xs
    
readInput = lines

lang :: [(Char, String)]
lang = [
    ('a', "@"),
    ('b', "8"),
    ('c', "("),
    ('d', "|)"),
    ('e', "3"),
    ('f', "#"),
    ('g', "6"),
    ('h', "[-]"),
    ('i', "|"),
    ('j', "_|"),
    ('k', "|<"),
    ('l', "1"),
    ('m', "[]\\/[]"),
    ('n', "[]\\[]"),
    ('o', "0"),
    ('p', "|D"),
    ('q', "(,)"),
    ('r', "|Z"),
    ('s', "$"),
    ('t', "']['"),
    ('u', "|_|"),
    ('v', "\\/"),
    ('w', "\\/\\/"),
    ('x', "}{"),
    ('y', "`/"),
    ('z', "2")]


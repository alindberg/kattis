#include <iostream>
#include <vector>

using namespace std;

typedef vector<int> vi;
typedef vector<bool> vb;
typedef vector<vi> vvi;


void explore(vvi &adj, vb &visited, int src) {
    
    visited[src] = true;

    for (int v : adj[src]) {
        if (!visited[v]) {
            explore(adj, visited, v);
        }
    }
}

int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)

    int n, m;
    cin >> n >> m;

    vvi adj(n + 1);

    for (int i = 0; i < m; i++) {
        int u, v;
        cin >> u >> v;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    vb visited(n + 1, false);

    explore(adj, visited, 1);

    bool con = false;
    for (int i = 1; i <= n; i++) {
        if (!visited[i]) {
            cout << i << "\n";
            con = true;
        }
    }

    if (!con) {
        cout << "Connected" << endl;
    }

}
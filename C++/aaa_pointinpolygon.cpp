#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

typedef pair<int, int> Point;
typedef vector<Point> Polygon;

#define x first
#define y second

enum Placement {
    in, on, out
};

string pl(Placement p) {
    if (p == 0) return "in";
    if (p == 1) return "on";
    return "out";
}

int dir(Point &a, Point &b, Point &c) {
    return (b.x - a.x) * (c.y - a.y) - (c.x - a.x) * (b.y - a.y);
}

Placement checkinside(Polygon &poly, Point &p) {
    
    int d;
    for (int i = 2; i < poly.size(); i++) {
        
        d = dir(poly[i-2], poly[i-1], p);

        if (d == 0) {
            return on;
        } else if (d < 0) {
            return out;
        }
    }

    d = dir(poly[poly.size()-1], poly[0], p);

    if (d == 0) {
        return on;
    } else if (d < 0) {
        return out;
    } else {
        return in;
    }

}

int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)
    
    int n, m, p;
    cin >> n;

    while (n) {

        Polygon poly(n);
        
        //determine whether cw or ccw 
        for (int i = 0; i < n; i++) {
            cin >> poly[i].x >> poly[i].y;
        }


        cin >> m;
        for (int j = 0; j < m; j++) {
            Point p;
            cin >> p.x >> p.y;
            cout << pl(checkinside(poly, p)) << endl;
        }

        cin >> n;
    }

    return 0;
}

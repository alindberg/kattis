import Data.Maybe
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as BC

main = do
    line <- B.getLine
    let nfactorial = BC.unpack line
    let len = length nfactorial
    
    
    putStrLn . show $ 
        if len < 7 
        then brute (readInt line) 
        else inv_fact len 
            

inv_fact :: Int -> Int
inv_fact l = go [1..] l 0 1
    where go _ n s i | ceiling s == n = i - 1
          go (x:xs) n s i = go xs n (s + logBase 10 x) (i + 1)


readInt :: B.ByteString -> Int
readInt = fst . fromJust . BC.readInt

brute :: Int -> Int 
brute x = go x 2
    where go 1 i = i - 1
          go x' i = go (div x' i) (i + 1)
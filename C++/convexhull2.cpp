#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <map>

using namespace std;

typedef vector<int> vi;
typedef vector<bool> vb;

typedef long long ll;

typedef pair<ll, ll> pll;
typedef vector<pll> vll;

#define x first
#define y second

bool ccw(pll &a, pll &b, pll &c) {
    return (b.x - a.x) * (c.y - a.y) - (c.x - a.x) * (b.y - a.y) < 0;
}

//assume P is sorted
vll convexhull(vll &points) {

    vll hull;
    hull.push_back(points[0]);
    hull.push_back(points[1]);

    for (int i = 2; i < points.size(); i++) {
        int n = hull.size();
        while (n >= 2 && ccw(hull[n - 2], hull[n - 1], points[i])) {
           hull.pop_back();
           n = hull.size();
        }
        hull.push_back(points[i]);
    }

    return hull;
}


vll solve(vll &points) {

    sort(points.begin(), points.end());

    vll reversed = vll(points.rbegin(), points.rend());

    vll lower = convexhull(points);
    vll upper = convexhull(reversed);

    lower.pop_back();
    upper.pop_back();

    lower.insert(lower.end(), upper.begin(), upper.end());
    return lower;
}


int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)
    //cout.tie(NULL);

    int n;
    cin >> n;

    vll points;

    for (int i = 0; i < n; i++) {
        int x, y;
        char c;
        cin >> x >> y >> c;

        if (c == 'Y') {
            points.push_back(pll(x, y));
        }
    }

    vll hull;

    if (points.size() > 1) {
        hull = solve(points);
    } else {
        hull = points;
    }

    cout << hull.size() << endl;
    for (int i = 0; i < hull.size(); i++) {
        cout << hull[i].x << " " << hull[i].y << endl;
    }

    return 0;
}
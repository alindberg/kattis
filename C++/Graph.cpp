#include <iostream>
#include "Graph.h"

Graph::Graph(int v):
    v(v),
    adj(vector<vector<int>>(v, vector<int>(0))) 
    {}

void Graph::addEdge(int u, int v) {
    adj[u].push_back(v);
    adj[v].push_back(u);
    e++;
}

vector<int> Graph::adjacent(int v) {
    return adj[v];
}

int Graph::vertices() {
    return v;
}

int Graph::edges() {
    return e;
}

string Graph::show() {
    string s;
    s += "Graph with " + 
        std::to_string(v) + " vertices, " +
        std::to_string(e) + " edges\n";

    for (int i = 0; i < v; i++) {
        s += std::to_string(i) + ": ";
        for(int k : adj[i]) {
            s += std::to_string(k) + " ";
        }
        
        if (i < v - 1)
            s += "\n";
    }

    return s;
}

#include <iostream>
#include <vector>

using namespace std;

typedef pair<int, int> pii;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<pii> vii;

#define value first
#define weight second

vi knap(vii &pairs, const int cap) {
    
    const int n = pairs.size();

    //dp[j][c] = max value achievable with ks of cap c.
    vvi dp(n + 1, vi(cap + 1, 0));

    for (int j = 1; j <= n; j++) {
        int wj = pairs[j-1].weight;
        int v = pairs[j-1].value;

        for(int c = 1; c <= cap; c++) {

            //the weight of the item is larger than the current capacity c
            if (wj > c) {
                dp[j][c] = dp[j-1][c];

            //the items weight is within the capacity.
            //see whether the knapsack is larger with or without this item
            } else {
                dp[j][c] = max(dp[j-1][c], dp[j-1][c-wj] + v);
            }
        }
    }

    //return the indices of the included items.
    vi indices;

    int result = dp[n][cap];
    int c = cap;

    for (int i = n - 1; i >= 0 && result > 0; i--) {
        
        if (result == dp[i][c]) {
            continue; 
        } else {
            //item included if dp[j-1][c-wj] + v was what happened.
            indices.push_back(i);
            result -= pairs[i].value;
            c -= pairs[i].weight;
        }

    }


    return indices;
}


int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)

    int c, n;
    while (cin >> c >> n) {
        
        vii pairs(n);
        for (int i = 0; i < n; i++) {
            cin >> pairs[i].value >> pairs[i].weight;
        }

        vi kn = knap(pairs, c);
        cout << kn.size() << endl;
        for (int i : kn) {
            cout << i << " ";
        }
        cout << endl;
    }

    return 0;
}
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

struct Vertex;
typedef vector<Vertex> Polygon;
typedef pair<int, int> pii;

struct Vertex {
    int x, y, index;

    bool operator <(const Vertex &rhs) const {
        return pii(x, y) < pii(rhs.x, rhs.y);
	}
};


bool clockwise(Vertex &a, Vertex &b, Vertex &c) {
    return (b.x - a.x) * (c.y - a.y) - (c.x - a.x) * (b.y - a.y) <= 0;
}

//assume P is sorted
Polygon convexhull(Polygon &P) {

    Polygon S;
    S.push_back(P[0]);
    S.push_back(P[1]);
    for (int i = 2; i < P.size(); i++) {

        while (S.size() >= 2 && !clockwise(S[S.size() - 2], S[S.size() - 1], P[i])) {
           S.pop_back();
        }
        S.push_back(P[i]);
    }

    return S;
}

int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)
    
    int test_cases;
    cin >> test_cases;

    for (int i = 0; i < test_cases; i++) {

        int n;
        cin >> n;

        Polygon polygon(n);

        for (int i = 0; i < n; i++) {
            cin >> polygon[i].x;
            cin >> polygon[i].y;
            polygon[i].index = i;
        }

        sort(polygon.begin(), polygon.end());

        Polygon upperwrap = convexhull(polygon);

        vector<bool> visited(n, false);

        for (Vertex v : upperwrap) {
            visited[v.index] = true;
        }


        //now walk from the back in descending order and add points not in the wrap
        for (int i = n - 1 ; i >= 0; i--) {
            if (!visited[polygon[i].index]) {
                upperwrap.push_back(polygon[i]);
            }
        }

        for (Vertex v : upperwrap) {
            cout << v.index << " ";
        }
        cout << endl;

    }

    return 0;
}

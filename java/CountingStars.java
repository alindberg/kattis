package kattis;

import java.util.ArrayList;
import java.util.List;

import Point2D;
import Graph;
import io.Kattio;


public class CountingStars {
	
	public CountingStars() {
		Kattio io = new Kattio(System.in, System.out);
		int k = 1; //Case counter. 1 <= k <= 50
		while(io.hasMoreTokens()) {
			int height = io.getInt(); // 
			int width = io.getInt();
			
			int clouds = 0;
			
			boolean[][] heaven = new boolean[height][width];
			for(int y = 0; y < height; y++) {
				String word = io.getWord();
				for(int x = 0; x < width; x++) {
					if(word.charAt(x) == '-') {
						heaven[y][x] = true;
					} else {
						clouds++;
					}
				}
			}
			io.println("Case " + k + ": " + countStars(heaven, clouds));
			k++;
		}
		io.close();
	}
	
		
	private int countStars(boolean[][] heaven, int clouds) {
		int h = heaven.length;
		int w = heaven[0].length;
		Graph graph = new Graph(w * h);
		
		for (int y = 0; y < h; y++) {
			for (int x = 0; x < w; x++) {
			
				if(heaven[y][x]) {
					Point2D p = new Point2D(x, y);
					int u = Point2D.toIndex(p, w);
					
					List<Integer> adj = toList(graph.adj(u)); //to be able to use contains.. hmm
	
					for (Point2D q : Point2D.neighbours4(p, w, h)) {
					
						int v = Point2D.toIndex(q, w);
						
						if (heaven[q.getY()][q.getX()] && !adj.contains(v)) {
							graph.addEdge(u, v);
						}
					}
				}
			}
		}
		
		DepthFirstCC a = new DepthFirstCC(graph);
		return a.count() - clouds;
	}
	
	public List<Integer> toList(Iterable<Integer> itr) {
		List<Integer> list = new ArrayList<>();
		itr.forEach(list::add);
		return list;
	}
		
	
	
	public static void main(String[] args) {
		new CountingStars();
	}
	
	
	
}

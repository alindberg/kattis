package kattis;

import Vector2D;
import io.Kattio;

public class Cetvrta {
	
	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		
		int x1 = io.getInt();
		int y1 = io.getInt();
		Vector2D OA = new Vector2D(x1, y1);
		
		int x2 = io.getInt();
		int y2 = io.getInt();
		Vector2D OB = new Vector2D(x2, y2);
		
		int x3 = io.getInt();
		int y3 = io.getInt();
		Vector2D OC = new Vector2D(x3, y3);
		
		Vector2D AB = new Vector2D(OA, OB);
		Vector2D BC = new Vector2D(OB, OC);
		Vector2D AC = new Vector2D(OA, OC);
		
		Vector2D mid = null;
		Vector2D point = null;
		if(Vector2D.dot(AC, BC) == 0) {

			mid = new Vector2D((OA.getX() + OB.getX()) / 2, (OA.getY() + OB.getY()) / 2);
			point = OC;
		} else if(Vector2D.dot(AB, BC) == 0) { 
			mid = new Vector2D((OA.getX() + OC.getX()) / 2, (OA.getY() + OC.getY()) / 2);
			point = OB;
		} else { 
			mid = new Vector2D((OB.getX() + OC.getX()) / 2, (OB.getY() + OC.getY()) / 2);
			point = OA;
		}
		
		Vector2D sol = solve(point, mid);
		
		io.println((int)sol.getX() + " " + (int)sol.getY());
		
		io.close();
	}
	
	static Vector2D solve(Vector2D a, Vector2D mid) {
		double dx = mid.getX() - a.getX(); 
		double dy = mid.getY() - a.getY(); 
		return new Vector2D(mid.getX() + dx, mid.getY() + dy);
	}

}

package kattis;

import java.util.Arrays;

import io.Kattio;

public class PhoneList {

	public static void main(String[] args) {

		Kattio io = new Kattio(System.in, System.out);
		
		int t = io.getInt(); //test cases
		for(int i = 0; i < t; i++) {
			int n = io.getInt(); // number of phone #'s.
			String[] nums = new String[n];
			for(int j = 0; j < n; j++) {
				nums[j] = io.getWord();
			}
			Arrays.sort(nums);
			boolean found = false;
			for(int k = nums.length - 1; k > 0; k--) {
				for(int l = 1; l < nums[k].length(); l++) {
					int s = Arrays.binarySearch(nums, nums[k].substring(0, l));
					if(s >= 0) {
						found = true;
					}
				}
			}
			io.println(found ? "NO" : "YES");
		}
		
		io.close();
	}

}

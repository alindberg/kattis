package kattis;

import io.Kattio;

public class RockPaperScissor {
	
	private static class Player {
		private int wins, losses;
		
		public void win() {wins++;}
		public void loss() {losses++;}
		public int getWins() {return wins;}
		public int getLosses() {return losses;}
	}

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		
		int N = io.getInt();
		
		while(N != 0) {
			Player[] players = new Player[N];
			for(int i = 0; i < N; i++) {
				players[i] = new Player();
			}
			
			int k = io.getInt(); 
			for(int i = 0; i < k * N * (N - 1) / 2; i++) {
				int id1 = io.getInt() - 1;
				String c1 = io.getWord();
				int id2 = io.getInt() - 1;
				String c2 = io.getWord();		
			
				if(c1.equals("rock") && c2.equals("paper")) {
					players[id1].loss();
					players[id2].win();
				} else if(c1.equals("rock") && c2.equals("scissors")) {
					players[id1].win();
					players[id2].loss();
				} else if(c1.equals("scissors") && c2.equals("paper")) {
					players[id1].win();
					players[id2].loss();
				} else if(c1.equals("scissors") && c2.equals("rock")) {
					players[id1].loss();
					players[id2].win();
				} else if(c1.equals("paper") && c2.equals("rock")) {
					players[id1].win();
					players[id2].loss();
				} else if(c1.equals("paper") && c2.equals("scissors")) { 
					players[id1].loss();
					players[id2].win();
				} else { //c1.equals(c2)..
				}
			}

			for (int i = 0; i < N; i++) {
				int wl = players[i].getWins() + players[i].getLosses();
				if (wl != 0) {

					System.out.printf("%.3f\n", (double)players[i].getWins() / wl);
				} else {
					System.out.println("-");
				}
			}
			System.out.println();
			N = io.getInt();
		}
		
		io.close();
	}

}

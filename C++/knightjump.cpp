#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include <queue>
#include <algorithm>

using namespace std;

typedef vector<int> vi;
typedef vector<bool> vb;
typedef vector<string> vs;

typedef pair<int, int> pii;
typedef vector<pii> vii;

#define inf (-1)
#define Empty '.'
#define Blocked '#'
#define Knight 'K'

#define x first
#define y second

int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)
    cout.tie(NULL);

    int N;
    cin >> N;

    vs board = vs(N, string(N, Empty));
    pii knight;

    for (int y = 0; y < N; y++) {
        string line;
        cin >> line;

        for (int x = 0; x < N; x++) {
            board[y][x] = line[x];
            if (board[y][x] == Knight) {
                knight = pii(x, y);
            }
        }
    }

    vector<vi> dist(N, vi(N, inf));
    queue<pii> Q;
    
    dist[knight.y][knight.x] = 0;
    Q.push(knight);
    
    while (!Q.empty()) {
        pii p = Q.front();
        Q.pop();

        pii pairs[8] = {
            pii(p.x - 1, p.y + 2),
            pii(p.x + 1, p.y + 2),
            pii(p.x - 2, p.y + 1),
            pii(p.x + 2, p.y + 1),
            pii(p.x - 2, p.y - 1),
            pii(p.x + 2, p.y - 1),
            pii(p.x - 1, p.y - 2),
            pii(p.x + 1, p.y - 2),
        };

        //each possible neighbor of p.. non visited ones.
        for (auto &b : pairs) {

            if (b.x >= 0 && b.x < N && b.y >= 0 && b.y < N 
                    && dist[b.y][b.x] == inf && board[b.y][b.x] != Blocked) {
                    
                dist[b.y][b.x] = dist[p.y][p.x] + 1;
                Q.push(b);
            }
        }

    }

    cout << dist[0][0] << endl;

    return 0;
}
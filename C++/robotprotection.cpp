#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

// Shortcuts for pair
#define x first
#define y second

typedef pair<int, int> Point;
typedef vector<Point> Polygon;

bool clockwise(Point &a, Point &b, Point &c) {
    return (b.x - a.x) * (c.y - a.y) - (c.x - a.x) * (b.y - a.y) <= 0;
}

Polygon hull(Polygon &P) {

    Polygon S;
    S.push_back(P[0]);
    S.push_back(P[1]);

    for (int i = 2; i < P.size(); i++) {

        while (S.size() >= 2 && clockwise(S[S.size() - 2], S[S.size() - 1], P[i])) {
            S.pop_back();
        }
        S.push_back(P[i]);
    }

    return S;
}

double area(Polygon &p) {

    double a = 0;

    for (int i = 1; i < p.size(); i++) {
        a += p[i-1].x * p[i].y - p[i].x * p[i-1].y;
    }

    return a + p[p.size() - 1].x * p[0].y - p[p.size() - 1].y * p[0].x;
}



double solve(Polygon &polygon) {

    if (polygon.size() < 3) {
        return 0;
    }

    sort(polygon.begin(), polygon.end());
    Polygon reversed = Polygon(polygon.rbegin(), polygon.rend());

    Polygon lower = hull(polygon);
    Polygon upper = hull(reversed);

    lower.pop_back();
    upper.pop_back();

    lower.insert(lower.end(), upper.begin(), upper.end());

    return 0.5 * area(lower);
}

int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)
    
    int beacons;
    cin >> beacons;

    while (beacons != 0) {

        Polygon poly(beacons);
        int x, y;
        
        for (int i = 0; i < beacons; i++) {
            cin >> x >> y;
            poly[i] = Point(x, y);
        }

        printf("%0.1f\n", solve(poly));

        cin >> beacons;
    }

    return 0;
}
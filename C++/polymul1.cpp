#include <iostream>
#include <vector>

using namespace std;


int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)

    int t;
    cin >> t;
    while(t--) {
        
        int n;
        cin >> n;
        vector<int> c(n+1, 1);
        for (int i = 0; i <= n; i++) cin >> c[i];

        int m;
        cin >> m;
        vector<int> k(m+1, 1);
        for (int j = 0; j <= m; j++) cin >> k[j];

        vector<int> result(n + m + 1, 0);

        for (int i = 0; i <= n; i++) {
            for (int j = 0; j <= m; j++) {
                result[i + j] += c[i] * k[j];
            }
        }
        
        cout << (n + m) << endl;
        for (int i = 0; i <= n + m; i++) {
            cout << result[i] << " ";
        }
        cout << endl;
    }

}
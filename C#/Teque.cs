using System;

public class Teque {

    public readonly int capacity;
    private int[] queue;
    private int head, tail;

    public Teque(int capacity) {
        this.capacity = capacity;
        queue = new int[capacity];
        tail = 0;
        size = 0;
        head = capacity - 1;
    }

    public void PushBack(int x) {
        queue[tail] = x;
        tail++;
    }

    public void PushFront(int x) {
        queue[head] = x;
        head--;
    }

    //push_middle x: insert the element x into the middle of the teque. 
    //The inserted element x now becomes the new middle element of the teque. 
    //If k is the size of the teque before the insertion, the insertion index for x is (k+1)/2 (using 0-based indexing).
    public void PushMid(int x) {

        //
        queue[mid] = x;
    }

    public int Size() {
        return head + (capacity - tail - 1);
    } 

    public int Mid() {
        return Size() / 2;
    }

    //Print ith index element (0-based)
    public void Print(int i) {
        
        Console.WriteLine("");
    }
    
    static void Main(string[] args) {
        
        int N = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("N = ", N);

        int x;
        string line;
        Teque teq = new Teque(1_000_000_000);
        for (int i = 0; i < N; i++) {

            line = Console.ReadLine();
            var words = line.Split(' ');
            x = Convert.ToInt32(words[1]);

            Console.WriteLine(line);
            
            switch (words[0]) {
                case "push_front":
                    teq.PushFront(x);
                case "push_back":
                    teq.PushBack(x);
                case "push_middle":
                    teq.PushMid(x);
                case "get":
                    teq.Print(x);
                default:
            }
        }
    }
}

//csc Teque.cs, Teque.exe
#include <iostream>
#include <cmath>

using namespace std;

int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)

    int num_digits[1000001];
    num_digits[0] = 1;
    num_digits[1] = 1;
    double s = 0;
    for (int i = 2; i <= 1000000; i++) {
        s += log10(i);
        num_digits[i] = (int)ceil(s);
    }

    int n;
    while (cin >> n) {
        cout << num_digits[n] << endl;
    }

    return 0;
}

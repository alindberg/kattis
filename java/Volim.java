package kattis;

import io.Kattio;

public class Volim {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		
		int ts = 210; //Total seconds..
		
		int k = io.getInt(); //Number of player initially holding the box.
		int q = io.getInt(); //Number of questions
		for(int i = 0; i < q; i++) {
			ts -= io.getInt(); 
			if(ts <= 0) {
				io.println(k);
				break;
			}

			if(io.getWord().charAt(0) == 'T') {
				k = k == 8 ? 1 : k + 1; //Pass to the left.				
			} 

		}
		
		io.close();
	}

}

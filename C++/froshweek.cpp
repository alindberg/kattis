#include <iostream>

using namespace std;

long swaps = 0;

void merge(int *a, int start, int end, int mid) {

    int t = 0;
    int l = start; 
    int r = mid + 1;
    int rightstart = r;
    int temp[end - start + 1];

    while (l <= mid && r <= end) {
        if (a[l] < a[r]) {
            temp[t] = a[l++];

        //This particular element a[r] must be swapped with all remaining on the left side.
        } else {
            temp[t] = a[r++];

            swaps += rightstart - l;
        } 

        t++;
    }

    copy(a + l, a + rightstart, temp + t);

    t += rightstart - l;

    copy(a + r, a + end + 1, temp + t);

    copy(temp, temp + t, a + start);
}

void sort(int *arr, int start, int end) {
    
    if (start < end) {
        int mid = (start + end) / 2;
        sort(arr, start, mid);
        sort(arr, mid + 1, end);

        merge(arr, start, end, mid);
    }
}


int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)

    int N;
    cin >> N;
    
    int *arr = new int[N];

    for (int i = 0; i < N; i++) {
        cin >> arr[i];
    }

    sort(arr, 0, N - 1);

    cout << swaps << endl;

    return 0;
}


/*
f.eks med

[1, 2, 7, 8] [3, 4, 9, 10]

når man når l = 2, r = 4, a[l] = 7 > a[r] = 3.
a[r] må swappes med alle element til høyre for a[l] i den venstre listen. 
*/
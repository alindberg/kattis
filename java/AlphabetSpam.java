package kattis;

import io.Kattio;

public class AlphabetSpam {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		
		int ws = 0; //whitespace
		int lc = 0; //lowercase
		int uc = 0; //uppercase
		int sy = 0; //symbols
		
		String input = io.getWord();
		int N = input.length();
		for(int i = 0; i < N; i++) {
			char c = input.charAt(i);
			if(c == '_') {
				ws++;
			} else if(Character.isAlphabetic(c)) {
				if(Character.isLowerCase(c)) {
					lc++;
				} else {
					uc++;
				}
			} else {
				sy++;
			}	
		}
		
		io.println((double)ws / N);
		io.println((double)lc / N);
		io.println((double)uc / N);
		io.println((double)sy / N);
		io.close();
	}

}

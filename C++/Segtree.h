#ifndef Segtree_H
#define Segtree_H

#include <vector>
#include <string>

using std::vector;
using std::string;

class Segtree {
    int N, offs;
    int* A;

public:
    Segtree(int *a, int n);
    int getSum(int i, int j);
    void update(int i, int v);
    void show();
};


#endif
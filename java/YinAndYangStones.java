package kattis;

import io.Kattio;

public class YinAndYangStones {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		
		String stones = io.getWord();
		char first = stones.charAt(0);
		int w = 0, b = 0;
		for(int i = 1; i < stones.length(); i++) {
			if(stones.charAt(i) == 'W') {
				w++;
			} else {
				b++;
			}
		}
		
		
		//One more white than black. Replace with a single white.
		if(w == b + 1 && first == 'B') {
			io.println(1);
		//One more black than white. Replace with a single black.
		} else if(b == w + 1 && first == 'W') {
			io.println(1);
		} else {
			io.println(0);
		}
		
		io.close();
	}

}

main = interact (writeOutput . solve . readInput)

readInput = words

writeOutput = show

solve :: [String] -> Int
solve (n:p:xs) = let x = read n :: Int
                     y = read p :: Int
                 in y

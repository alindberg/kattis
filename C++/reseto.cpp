#include <iostream>
#include <vector>

using namespace std;

int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)

    int n, k;
    cin >> n >> k;

    vector<bool> nums(n + 1, true);

    int P = 2;
    int i = 0;
    while (i < k) {

        while (!nums[P]) P++; 

        int j = P;

        for (int j = P; j <= n; j += P) {

            if (nums[j]) {
                nums[j] = false;
                i++;

                if (i == k) {
                    cout << j << endl;
                    break;
                }
            }
        }
    }

    return 0;
}
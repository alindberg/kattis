package kattis;

import io.Kattio;

public class Tarifa {

	public static void main(String[] args) {
		Kattio io = new Kattio(System.in, System.out);
		int x = io.getInt();
		int n = io.getInt();
		int sum = x;
		for(int i = 0; i < n; i++) {
			sum += x;
			sum -= io.getInt();
		}
		System.out.println(sum);
		io.close();
	}


}

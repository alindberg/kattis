#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

void swapVertically(int x, int y, int dy, int grid[4][4]) {
    int tmp = grid[y][x];
    grid[y][x] = grid[dy][x];
    grid[dy][x] = tmp;
}

void swapHorizontally(int x, int y, int dx, int grid[4][4]) {
    int tmp = grid[y][x];
    grid[y][x] = grid[y][dx];
    grid[y][dx] = tmp;
}


void solve(int grid[4][4], int move) {
    
    const int left = 0, up = 1, right = 2, down = 3;

    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            for (int k = j + 1; k < 4; k++) {

                int x, y, dx, dy;

                switch (move) {
                    case left:
                        x = j, dx = k, y = i, dy = i;
                        break;
                    case right:
                        x = 3 - j, dx = 3 - k, y = i, dy = i;
                        break;
                    case up:
                        x = i, dx = i, y = j, dy = k;
                        break;
                    case down:
                        x = i, dx = i, y = 3 - j, dy = 3 - k; 
                        break;
                }

                
                if (grid[y][x] == 0) {
                    if (move == left || move == right) {
                        swapHorizontally(x, y, dx, grid);
                    } else {
                        swapVertically(x, y, dy, grid);
                    }
                } else if (grid[dy][dx] == 0) {
                    continue;
                } else if (grid[y][x] == grid[dy][dx]) {
                    grid[y][x] = 2 * grid[dy][x];
                    grid[dy][dx] = 0;
                    break;
                } else if (grid[y][x] != grid[dy][dx]) {
                    break;
                }
            }   

        }
    }
}


int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)
    cout.tie(NULL);

    int grid[4][4];
    for (int y = 0; y < 4; y++) {
        for (int x = 0; x < 4; x++) {
            cin >> grid[y][x];
        }        
    }

    int move;
    cin >> move;

    solve(grid, move);

    for (int y = 0; y < 4; y++) {
        for (int x = 0; x < 4; x++) {
            cout << grid[y][x] << " ";
        }
        cout << endl;
    }

    return 0;
}
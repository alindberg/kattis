package kattis;

import java.util.Scanner;

public class QuickBrownFox {

	private final static String ALPH = "abcdefghijklmnopqrstuvwxyz";
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int n = Integer.parseInt(sc.nextLine());
		String[] lines = new String[n];
		for(int i = 0; i < n; i++) {
			lines[i] = sc.nextLine().toLowerCase();
		}
		
		for(int i = 0; i < n; i++) {
			int[] occ = new int[ALPH.length()];
			for(int j = 0; j < lines[i].length(); j++) {
				if(Character.isAlphabetic(lines[i].charAt(j))) {
					occ[lines[i].charAt(j) - 'a']++;
				} 
			}
			
			StringBuilder sb = new StringBuilder();
			for(int k = 0; k < occ.length; k++) {
				if(occ[k] == 0) {
					sb.append(ALPH.charAt(k));
				}
			}
			if(sb.length() == 0) {
				System.out.println("pangram");
			} else {
				System.out.println("missing " + sb.toString());
			}
		}
		
		sc.close();
	}

}

#include <iostream>
#include "SegtreeIQ.h"

using namespace std;

int main() {
    ios::sync_with_stdio(false); // Speedup (do not mix with scanf/prinf)
    cin.tie(NULL); // Speedup (do not mix with scanf/prinf)

    int N, K, Q;
    cin >> N >> K >> Q;

    SegtreeIQ segtree(N);
    
    for (int i = 0; i < K + Q; i++) {
        char c;
        cin >> c;
        if (c == '!') {
            int l, r, d;
            cin >> l >> r >> d;

            segtree.update(l, r - 1, d);

        } else if (c == '?') {
            int x;
            cin >> x;

            cout << segtree.getValue(x - 1) << endl;
        }
    }

    return 0;
}